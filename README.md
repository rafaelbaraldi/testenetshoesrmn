# README #

App Teste - Netshoes (Rodrigo Mendes Nunes)

-------------------------------------------------------------------------------------

*Telas:*

**LISTA** de gists usando https://api.github.com/gists/public?page=0 (OK)

A cada celula deve conter:

- Nome do criador ("owner") do gist
- Foto do criador ("owner") do gist
- Tipo do gist (application/xml, text/plain, etc)
- Linguagem do gist (Objective-C, Shell, Text, etc)

![Simulator Screen Shot Jun 3, 2016, 1.15.51 PM.png](https://bitbucket.org/repo/B7XgGG/images/3259495512-Simulator%20Screen%20Shot%20Jun%203,%202016,%201.15.51%20PM.png)


**DETAIL** de gist (use os dados dentro da lista). (OK)

Tela deve conter:

- Nome do criador ("owner") do gist
- Foto do criador ("owner") do gist
- Gist completo (Pelo meu entendimento, utilizei a request "raw_url" para exibição)

![Simulator Screen Shot Jun 3, 2016, 1.16.05 PM.png](https://bitbucket.org/repo/B7XgGG/images/224011850-Simulator%20Screen%20Shot%20Jun%203,%202016,%201.16.05%20PM.png)


-------------------------------------------------------------------------------------

*Requisitos do projeto:*

- Suporte para iOS 9 (OK)
- Pelo menos um view controller em Swift (NOK)
- Lista tem que ser um collection view (NOK)
- Usar Storyboard e Autolayout (OK)
- Gestão de dependências no projeto. Ex: Cocoapods (OK)
- Framework para Comunicação com API. Ex: AFNetworking (OK)
- Mapeamento JSON -&gt; Objeto . Ex: Mantle, JSONHelper.swift (OK)
- Paginação automática (scroll infinito) na tela de lista (OK)
- Tela de detalhe de um produto ao clicar em um item da lista (OK)
- Mínimo de um teste unitário (OK)
- Menos de 4 dependências (OK)
- Projeto feito 100% em Swift (NOK)

**OBS.:**

- Utilizei o programa JSON Accelerator para criar as classes de dados, conforme
- retorno da chamada da request https://api.github.com/gists/public?page=0
- Manipulei a classe "ACCGists" para adequar ao retorno da seção "files" do JSON
- Plus: adicionei o scroll acima (UIRefreshControl)


Ganha + pontos:
- Funciona Offline (NOK)