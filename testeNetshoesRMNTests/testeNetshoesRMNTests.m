//
//  testeNetshoesRMNTests.m
//  testeNetshoesRMNTests
//
//  Created by Rodrigo Mendes Nunes on 31/05/16.
//  Copyright © 2016 Rodrigo Mendes Nunes. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BaseManager.h"

@interface testeNetshoesRMNTests : XCTestCase

@end

@implementation testeNetshoesRMNTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    
    XCTAssert(YES, @"Pass");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

- (void)testRequestGetGists {
    XCTestExpectation *completionExpectation = [self expectationWithDescription:@"Long method"];
    
    // Request para buscar a lista de gist
    [BaseManager getGists:0 onSucces:^(NSArray* respObj) {
        
        XCTAssertGreaterThan(respObj.count, 0);
        [completionExpectation fulfill];
        
    } onError:^(id err) {
    }];
    
    [self waitForExpectationsWithTimeout:5.0 handler:nil];
}


@end
