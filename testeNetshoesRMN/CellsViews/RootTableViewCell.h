//
//  RootTableViewCell.h
//  testeNetshoesRMN
//
//  Created by Rodrigo Mendes Nunes on 01/06/16.
//  Copyright © 2016 Rodrigo Mendes Nunes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgFoto;
@property (weak, nonatomic) IBOutlet UILabel *lblNome;
@property (weak, nonatomic) IBOutlet UILabel *lblLinguagem;
@property (weak, nonatomic) IBOutlet UILabel *lblTipo;

@end
