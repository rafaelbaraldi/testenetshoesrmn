//
//  RootTableViewController.h
//  testeNetshoesRMN
//
//  Created by Rodrigo Mendes Nunes on 01/06/16.
//  Copyright © 2016 Rodrigo Mendes Nunes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataModelsGists.h"

@interface RootTableViewController : UITableViewController

@property NSMutableArray* arrayGists;

@property int page;

@property (nonatomic) UIRefreshControl *refreshControlSpinner;

@end
