//
//  RootTableViewController.m
//  testeNetshoesRMN
//
//  Created by Rodrigo Mendes Nunes on 01/06/16.
//  Copyright © 2016 Rodrigo Mendes Nunes. All rights reserved.
//

#import "RootTableViewController.h"
#import "BaseManager.h"
#import "RootTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ChildGistViewController.h"

const int kLoadingCellTag = 1273;

@interface RootTableViewController ()

@end

@implementation RootTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    // Seta o título da página
    self.title = @"Gists (teste RMN)";
    
    // Inicializa o array de gists
    _arrayGists = [NSMutableArray new];
    
    // Inicializa a página
    _page = 0;
    
    // Scroll acima
    _refreshControlSpinner = [[UIRefreshControl alloc] init];
    [_refreshControlSpinner addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:_refreshControlSpinner];
    
    // Realiza a request
    [self rootRequest];
}

- (void)refresh:(UIRefreshControl *)refreshControl {
    
    // Realiza a request
    [self rootRequest];
}

-(void)rootRequest{
    
    // Request para buscar a lista de gist
    [BaseManager getGists:_page onSucces:^(NSArray* respObj) {
        
        [_arrayGists removeAllObjects];
        
        NSArray *responseArray = respObj;
        
        // Percorre toda a lista recebida
        for (int i = 0; i < [responseArray count]; i++) {
            
            // Pega o item da lista
            ACCGists* gist = [[ACCGists alloc]initWithDictionary:[responseArray objectAtIndex:i]];
            
            // CONDIÇÕES:
            // 1) Exibe apenas os itens onde existe um owner
            // 2) Exibe todos os files do owner
            if(gist.owner.login){
                
                // Preenche o array com todos os files do owner
                gist.filesArray = [((NSDictionary*)gist.files) allValues];
                
                // Percorre todos os files do array de files
                for(NSDictionary* file in gist.filesArray){
                    
                    // Pega a struct do file
                    ACCStructFile* accFile = [[ACCStructFile alloc] initWithDictionary:file];
                    
                    // Se for o primeiro file
                    if ([gist.filesArray indexOfObject:file] == 0) {
                        
                        // Seta o file atual para o file do item da lista
                        gist.file = accFile;
                      
                        // Adiciona no array de gists da tela
                        [_arrayGists addObject:gist];
                    }
                    // Para os outros files, "replicamos" os dados do gist com o file atual
                    else{
                        // Cria um novo gist (cópia) para adiciona na lista.
                        ACCGists* newGist = [gist copy];
                        
                        // Seta o file atual para o file do item da lista
                        newGist.file = accFile;
                        
                        // Adiciona no array de gists da tela
                        [_arrayGists addObject:newGist];
                    }
                }
            }
            
        }
        
        [self.tableView reloadData];
        [_refreshControlSpinner endRefreshing];
        
    } onError:^(id err) {
        
        [_refreshControlSpinner endRefreshing];
        // Exibe a mensagem de erro
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Erro de conexão" message:[err localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        
        [self presentViewController:alert animated:YES completion:nil];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Controles de paginação
    if (_page < _arrayGists.count) {
        return _arrayGists.count + 1;
    }
    
    return _arrayGists.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row < _arrayGists.count) {
  
        return [self rootCellForIndexPath:indexPath tableView:tableView];
    }
    else {
        
        return [self loadingCell];
    }
}


- (RootTableViewCell *)rootCellForIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView {
    
    RootTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"rootCell" forIndexPath:indexPath];
    
    if(!cell){
        cell = [[RootTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"rootCell"];
    }
    
    // Cria o modelo do gist da célula
    ACCGists *gist = [ACCGists new];
    gist = [_arrayGists objectAtIndex:indexPath.row];
    
    // Baixa a foto através da url utilizando a biblioteca SDWebImage
    [cell.imgFoto sd_setImageWithURL:[NSURL URLWithString:gist.owner.avatarUrl] placeholderImage:[UIImage imageNamed:@"userPlaceholder"]];
    
    // Preenche os dados dos labels
    cell.lblNome.text = [NSString stringWithFormat:@"%@ / %@", gist.owner.login, gist.file.filename];
    
    cell.lblTipo.text = [NSString stringWithFormat:@"Type: %@", gist.file.type];
    cell.lblLinguagem.text = [NSString stringWithFormat:@"Language: %@", gist.file.language];
    
    return cell;
}

- (UITableViewCell *)loadingCell {
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    activityIndicator.center = cell.center;
    [cell addSubview:activityIndicator];
    
    [activityIndicator startAnimating];
    
    cell.tag = kLoadingCellTag;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // Controle de paginação
    if (cell.tag == kLoadingCellTag) {
        _page++;
        
        // REaliza uma nova request
        [self rootRequest];
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    // Passa para a tela de detalhes
    if ([segue.identifier isEqualToString:@"rootSegue"]) {
        ChildGistViewController* childController = (ChildGistViewController*)segue.destinationViewController;
        childController.gist = [_arrayGists objectAtIndex:[self.tableView indexPathForSelectedRow].row];
    }
}


@end
