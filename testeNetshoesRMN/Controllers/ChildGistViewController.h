//
//  ChildGistViewController.h
//  testeNetshoesRMN
//
//  Created by Rodrigo Mendes Nunes on 6/3/16.
//  Copyright © 2016 Rodrigo Mendes Nunes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataModelsGists.h"

@interface ChildGistViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblNomeOwnerGist;
@property (weak, nonatomic) IBOutlet UILabel *lblNomeArquivo;
@property (weak, nonatomic) IBOutlet UIImageView *imgFoto;
@property (weak, nonatomic) IBOutlet UITextView *txtRaw;

@property ACCGists* gist;

@end
