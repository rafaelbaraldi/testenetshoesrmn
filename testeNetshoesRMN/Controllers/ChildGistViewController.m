//
//  ChildGistViewController.m
//  testeNetshoesRMN
//
//  Created by Rodrigo Mendes Nunes on 6/3/16.
//  Copyright © 2016 Rodrigo Mendes Nunes. All rights reserved.
//

#import "ChildGistViewController.h"
#import "BaseManager.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface ChildGistViewController ()

@end

@implementation ChildGistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Preenche os dados dos labels 
    _lblNomeOwnerGist.text = [NSString stringWithFormat:@"%@ / %@", _gist.owner.login, _gist.file.filename];
    _lblNomeArquivo.text = [NSString stringWithFormat:@"%@", _gist.file.filename];
    
    // Baixa a foto através da url utilizando a biblioteca SDWebImage
    [_imgFoto sd_setImageWithURL:[NSURL URLWithString:_gist.owner.avatarUrl] placeholderImage:[UIImage imageNamed:@"userPlaceholder"]];
    
    // Realiza a request
    [self childRequest];
}

-(void)childRequest{
    
    // Request para buscar o detalhe (raw) do gist selecionado
    [BaseManager getGistRaw:_gist.file.rawUrl onSuccess:^(id respObj) {
        
        NSString *responseString = [[NSString alloc] initWithData:respObj encoding:NSUTF8StringEncoding];
        
        NSLog(@"%@", responseString);
        
        // Preenche o campo raw da tela
        _txtRaw.text = responseString;
        
    } onError:^(id err) {
        
        // Exibe a mensagem de erro
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Erro de conexão" message:[err localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        
        [self presentViewController:alert animated:YES completion:nil];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
