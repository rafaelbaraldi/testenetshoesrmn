//
//  ViewController.m
//  testeNetshoesRMN
//
//  Created by Rodrigo Mendes Nunes on 31/05/16.
//  Copyright © 2016 Rodrigo Mendes Nunes. All rights reserved.
//

#import "ViewController.h"
#import <AFNetworking/AFNetworking.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
//    [self requestGists];
    
//    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//    [manager GET:kUrlListaGists parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
//        NSLog(@"JSON: %@", responseObject);
//    } failure:^(NSURLSessionTask *operation, NSError *error) {
//        NSLog(@"Error: %@", error);
//    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - REQUEST

-(void)requestGists{
    
//    NSString* url = nil;
//    url = kUrlListaGists;
//    url = [[[Singleton sharedInstance].parametros getValueUrls:kValueUrlSERVICOS] stringByAppendingString:kUrlListaVendas];
    
//    NSDictionary* jsonDict = [NSDictionary dictionaryWithObjectsAndKeys:
//                              @([Singleton sharedInstance].parametros.loja.codigo), @"codigo_loja",
//                              @(_qtdeItensBusca), @"quantidade",
//                              nil];
    
//    NSString* jsonQuery = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:jsonDict options:kNilOptions error:nil] encoding:NSUTF8StringEncoding];
    
//    [[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET" URLString:url parameters:nil error:nil];
    
    NSDictionary* jsonDictLista = [NSDictionary new];
    
    [self doRequest:kUrlListaGists getDictionary:jsonDictLista onSuccess:^(id respObj) {
//    [self doRequest:url getDictionary:[NSDictionary dictionaryWithObject:jsonQuery forKey:@"parametros_lista"] onSuccess:^(id respObj) {
    
        if(respObj != nil){
            
            if ([respObj isKindOfClass:[NSArray class]]) {
                NSArray *responseArray = respObj;
                /* do something with responseArray */
            } else if ([respObj isKindOfClass:[NSDictionary class]]) {
                NSDictionary *responseDict = respObj;
                /* do something with responseDict */
                
                ACCGists* listaGists = [[ACCGists alloc] initWithDictionary:responseDict];
            }
            
            
//
            if (respObj) {
//
//                [_listaVendas removeAllObjects];
//                
//                [_listaVendas addObjectsFromArray:listaVendasADM.vendas];
//                
//                [_tableViewListaVendas reloadData];
            }
            else{
                
                UIAlertController * alert=   [UIAlertController
                                              alertControllerWithTitle:@"Erro"
                                              message:@"Erro"
                                              preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert animated:YES completion:nil];
            }
             
            
        }
        else{
            
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:@"Erro"
                                          message:@"Não existem itens para exibição."
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
     
    } onError:^(NSError *err) {
        
        // recupera os dados da base offline
//        [self recuperaDadosOffline];
//        [_tableViewListaVendas reloadData];
        
        //        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Erro de conexão"
        //                                                            message:[err localizedDescription]
        //                                                           delegate:self
        //                                                  cancelButtonTitle:@"Ok"
        //                                                  otherButtonTitles:nil];
        //        [alertView show];
    } ];
}

- (void)doRequest:(NSString *)uriString getDictionary:(NSDictionary*)get onSuccess:(void (^)(id respObj))successFuction
          onError:(void (^)(id err))errorFuction
{
//    LoadView *loadView = [LoadView new];
//    [loadView startLoadAnimationOnView:_viewPrincipal];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager GET:uriString parameters:get progress:nil
         success:^(NSURLSessionTask *task, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
//         NSLog(@"STATUSCODE: %li", (long)task.response.statusCode);
         successFuction( responseObject ) ;
//         [loadView stopLoadAnimationOnView:_viewPrincipal];
     }
         failure:
     ^(NSURLSessionTask *operation, NSError *error) {
         errorFuction(  error );
//         [loadView stopLoadAnimationOnView:_viewPrincipal];
         NSLog(@"Error: %@", error);
     }];
}

@end
