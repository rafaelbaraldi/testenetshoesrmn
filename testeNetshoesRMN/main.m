//
//  main.m
//  testeNetshoesRMN
//
//  Created by Rodrigo Mendes Nunes on 31/05/16.
//  Copyright © 2016 Rodrigo Mendes Nunes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
