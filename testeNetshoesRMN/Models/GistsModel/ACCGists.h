//
//  ACCGists.h
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ACCStructFile.h"

@class ACCOwner, ACCFiles;

@interface ACCGists : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *internalBaseClassIdentifier;
@property (nonatomic, assign) double comments;
@property (nonatomic, strong) NSString *internalBaseClassDescription;
@property (nonatomic, strong) NSString *createdAt;
@property (nonatomic, strong) ACCOwner *owner;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *gitPushUrl;
//@property (nonatomic, strong) ACCFiles *files;
@property (nonatomic, strong) NSDictionary *files;
@property (nonatomic, strong) NSArray *filesArray;
@property (nonatomic, strong) NSString *htmlUrl;
@property (nonatomic, strong) NSString *gitPullUrl;
@property (nonatomic, assign) BOOL truncated;
@property (nonatomic, strong) NSString *updatedAt;
@property (nonatomic, strong) NSString *forksUrl;
@property (nonatomic, strong) NSString *commitsUrl;
@property (nonatomic, assign) id user;
@property (nonatomic, assign) BOOL publicProperty;
@property (nonatomic, strong) NSString *commentsUrl;

@property (nonatomic, strong) ACCStructFile *file;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
