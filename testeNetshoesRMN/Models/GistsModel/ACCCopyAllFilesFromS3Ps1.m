//
//  ACCCopyAllFilesFromS3Ps1.m
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ACCCopyAllFilesFromS3Ps1.h"


NSString *const kACCCopyAllFilesFromS3Ps1RawUrl = @"raw_url";
NSString *const kACCCopyAllFilesFromS3Ps1Language = @"language";
NSString *const kACCCopyAllFilesFromS3Ps1Filename = @"filename";
NSString *const kACCCopyAllFilesFromS3Ps1Size = @"size";
NSString *const kACCCopyAllFilesFromS3Ps1Type = @"type";


@interface ACCCopyAllFilesFromS3Ps1 ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ACCCopyAllFilesFromS3Ps1

@synthesize rawUrl = _rawUrl;
@synthesize language = _language;
@synthesize filename = _filename;
@synthesize size = _size;
@synthesize type = _type;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.rawUrl = [self objectOrNilForKey:kACCCopyAllFilesFromS3Ps1RawUrl fromDictionary:dict];
            self.language = [self objectOrNilForKey:kACCCopyAllFilesFromS3Ps1Language fromDictionary:dict];
            self.filename = [self objectOrNilForKey:kACCCopyAllFilesFromS3Ps1Filename fromDictionary:dict];
            self.size = [[self objectOrNilForKey:kACCCopyAllFilesFromS3Ps1Size fromDictionary:dict] doubleValue];
            self.type = [self objectOrNilForKey:kACCCopyAllFilesFromS3Ps1Type fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.rawUrl forKey:kACCCopyAllFilesFromS3Ps1RawUrl];
    [mutableDict setValue:self.language forKey:kACCCopyAllFilesFromS3Ps1Language];
    [mutableDict setValue:self.filename forKey:kACCCopyAllFilesFromS3Ps1Filename];
    [mutableDict setValue:[NSNumber numberWithDouble:self.size] forKey:kACCCopyAllFilesFromS3Ps1Size];
    [mutableDict setValue:self.type forKey:kACCCopyAllFilesFromS3Ps1Type];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.rawUrl = [aDecoder decodeObjectForKey:kACCCopyAllFilesFromS3Ps1RawUrl];
    self.language = [aDecoder decodeObjectForKey:kACCCopyAllFilesFromS3Ps1Language];
    self.filename = [aDecoder decodeObjectForKey:kACCCopyAllFilesFromS3Ps1Filename];
    self.size = [aDecoder decodeDoubleForKey:kACCCopyAllFilesFromS3Ps1Size];
    self.type = [aDecoder decodeObjectForKey:kACCCopyAllFilesFromS3Ps1Type];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_rawUrl forKey:kACCCopyAllFilesFromS3Ps1RawUrl];
    [aCoder encodeObject:_language forKey:kACCCopyAllFilesFromS3Ps1Language];
    [aCoder encodeObject:_filename forKey:kACCCopyAllFilesFromS3Ps1Filename];
    [aCoder encodeDouble:_size forKey:kACCCopyAllFilesFromS3Ps1Size];
    [aCoder encodeObject:_type forKey:kACCCopyAllFilesFromS3Ps1Type];
}

- (id)copyWithZone:(NSZone *)zone
{
    ACCCopyAllFilesFromS3Ps1 *copy = [[ACCCopyAllFilesFromS3Ps1 alloc] init];
    
    if (copy) {

        copy.rawUrl = [self.rawUrl copyWithZone:zone];
        copy.language = [self.language copyWithZone:zone];
        copy.filename = [self.filename copyWithZone:zone];
        copy.size = self.size;
        copy.type = [self.type copyWithZone:zone];
    }
    
    return copy;
}


@end
