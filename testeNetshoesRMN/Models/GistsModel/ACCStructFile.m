//
//  ACCStructFile.m
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ACCStructFile.h"


NSString *const kACCTestRbRawUrl = @"raw_url";
NSString *const kACCTestRbLanguage = @"language";
NSString *const kACCTestRbFilename = @"filename";
NSString *const kACCTestRbSize = @"size";
NSString *const kACCTestRbType = @"type";


@interface ACCStructFile ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ACCStructFile

@synthesize rawUrl = _rawUrl;
@synthesize language = _language;
@synthesize filename = _filename;
@synthesize size = _size;
@synthesize type = _type;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.rawUrl = [self objectOrNilForKey:kACCTestRbRawUrl fromDictionary:dict];
            self.language = [self objectOrNilForKey:kACCTestRbLanguage fromDictionary:dict];
            self.filename = [self objectOrNilForKey:kACCTestRbFilename fromDictionary:dict];
            self.size = [[self objectOrNilForKey:kACCTestRbSize fromDictionary:dict] doubleValue];
            self.type = [self objectOrNilForKey:kACCTestRbType fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.rawUrl forKey:kACCTestRbRawUrl];
    [mutableDict setValue:self.language forKey:kACCTestRbLanguage];
    [mutableDict setValue:self.filename forKey:kACCTestRbFilename];
    [mutableDict setValue:[NSNumber numberWithDouble:self.size] forKey:kACCTestRbSize];
    [mutableDict setValue:self.type forKey:kACCTestRbType];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.rawUrl = [aDecoder decodeObjectForKey:kACCTestRbRawUrl];
    self.language = [aDecoder decodeObjectForKey:kACCTestRbLanguage];
    self.filename = [aDecoder decodeObjectForKey:kACCTestRbFilename];
    self.size = [aDecoder decodeDoubleForKey:kACCTestRbSize];
    self.type = [aDecoder decodeObjectForKey:kACCTestRbType];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_rawUrl forKey:kACCTestRbRawUrl];
    [aCoder encodeObject:_language forKey:kACCTestRbLanguage];
    [aCoder encodeObject:_filename forKey:kACCTestRbFilename];
    [aCoder encodeDouble:_size forKey:kACCTestRbSize];
    [aCoder encodeObject:_type forKey:kACCTestRbType];
}

- (id)copyWithZone:(NSZone *)zone
{
    ACCStructFile *copy = [[ACCStructFile alloc] init];
    
    if (copy) {

        copy.rawUrl = [self.rawUrl copyWithZone:zone];
        copy.language = [self.language copyWithZone:zone];
        copy.filename = [self.filename copyWithZone:zone];
        copy.size = self.size;
        copy.type = [self.type copyWithZone:zone];
    }
    
    return copy;
}


@end
