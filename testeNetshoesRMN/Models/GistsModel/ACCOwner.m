//
//  ACCOwner.m
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ACCOwner.h"


NSString *const kACCOwnerId = @"id";
NSString *const kACCOwnerOrganizationsUrl = @"organizations_url";
NSString *const kACCOwnerReceivedEventsUrl = @"received_events_url";
NSString *const kACCOwnerFollowingUrl = @"following_url";
NSString *const kACCOwnerLogin = @"login";
NSString *const kACCOwnerSubscriptionsUrl = @"subscriptions_url";
NSString *const kACCOwnerAvatarUrl = @"avatar_url";
NSString *const kACCOwnerUrl = @"url";
NSString *const kACCOwnerType = @"type";
NSString *const kACCOwnerReposUrl = @"repos_url";
NSString *const kACCOwnerHtmlUrl = @"html_url";
NSString *const kACCOwnerEventsUrl = @"events_url";
NSString *const kACCOwnerSiteAdmin = @"site_admin";
NSString *const kACCOwnerStarredUrl = @"starred_url";
NSString *const kACCOwnerGistsUrl = @"gists_url";
NSString *const kACCOwnerGravatarId = @"gravatar_id";
NSString *const kACCOwnerFollowersUrl = @"followers_url";


@interface ACCOwner ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ACCOwner

@synthesize ownerIdentifier = _ownerIdentifier;
@synthesize organizationsUrl = _organizationsUrl;
@synthesize receivedEventsUrl = _receivedEventsUrl;
@synthesize followingUrl = _followingUrl;
@synthesize login = _login;
@synthesize subscriptionsUrl = _subscriptionsUrl;
@synthesize avatarUrl = _avatarUrl;
@synthesize url = _url;
@synthesize type = _type;
@synthesize reposUrl = _reposUrl;
@synthesize htmlUrl = _htmlUrl;
@synthesize eventsUrl = _eventsUrl;
@synthesize siteAdmin = _siteAdmin;
@synthesize starredUrl = _starredUrl;
@synthesize gistsUrl = _gistsUrl;
@synthesize gravatarId = _gravatarId;
@synthesize followersUrl = _followersUrl;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.ownerIdentifier = [[self objectOrNilForKey:kACCOwnerId fromDictionary:dict] doubleValue];
            self.organizationsUrl = [self objectOrNilForKey:kACCOwnerOrganizationsUrl fromDictionary:dict];
            self.receivedEventsUrl = [self objectOrNilForKey:kACCOwnerReceivedEventsUrl fromDictionary:dict];
            self.followingUrl = [self objectOrNilForKey:kACCOwnerFollowingUrl fromDictionary:dict];
            self.login = [self objectOrNilForKey:kACCOwnerLogin fromDictionary:dict];
            self.subscriptionsUrl = [self objectOrNilForKey:kACCOwnerSubscriptionsUrl fromDictionary:dict];
            self.avatarUrl = [self objectOrNilForKey:kACCOwnerAvatarUrl fromDictionary:dict];
            self.url = [self objectOrNilForKey:kACCOwnerUrl fromDictionary:dict];
            self.type = [self objectOrNilForKey:kACCOwnerType fromDictionary:dict];
            self.reposUrl = [self objectOrNilForKey:kACCOwnerReposUrl fromDictionary:dict];
            self.htmlUrl = [self objectOrNilForKey:kACCOwnerHtmlUrl fromDictionary:dict];
            self.eventsUrl = [self objectOrNilForKey:kACCOwnerEventsUrl fromDictionary:dict];
            self.siteAdmin = [[self objectOrNilForKey:kACCOwnerSiteAdmin fromDictionary:dict] boolValue];
            self.starredUrl = [self objectOrNilForKey:kACCOwnerStarredUrl fromDictionary:dict];
            self.gistsUrl = [self objectOrNilForKey:kACCOwnerGistsUrl fromDictionary:dict];
            self.gravatarId = [self objectOrNilForKey:kACCOwnerGravatarId fromDictionary:dict];
            self.followersUrl = [self objectOrNilForKey:kACCOwnerFollowersUrl fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.ownerIdentifier] forKey:kACCOwnerId];
    [mutableDict setValue:self.organizationsUrl forKey:kACCOwnerOrganizationsUrl];
    [mutableDict setValue:self.receivedEventsUrl forKey:kACCOwnerReceivedEventsUrl];
    [mutableDict setValue:self.followingUrl forKey:kACCOwnerFollowingUrl];
    [mutableDict setValue:self.login forKey:kACCOwnerLogin];
    [mutableDict setValue:self.subscriptionsUrl forKey:kACCOwnerSubscriptionsUrl];
    [mutableDict setValue:self.avatarUrl forKey:kACCOwnerAvatarUrl];
    [mutableDict setValue:self.url forKey:kACCOwnerUrl];
    [mutableDict setValue:self.type forKey:kACCOwnerType];
    [mutableDict setValue:self.reposUrl forKey:kACCOwnerReposUrl];
    [mutableDict setValue:self.htmlUrl forKey:kACCOwnerHtmlUrl];
    [mutableDict setValue:self.eventsUrl forKey:kACCOwnerEventsUrl];
    [mutableDict setValue:[NSNumber numberWithBool:self.siteAdmin] forKey:kACCOwnerSiteAdmin];
    [mutableDict setValue:self.starredUrl forKey:kACCOwnerStarredUrl];
    [mutableDict setValue:self.gistsUrl forKey:kACCOwnerGistsUrl];
    [mutableDict setValue:self.gravatarId forKey:kACCOwnerGravatarId];
    [mutableDict setValue:self.followersUrl forKey:kACCOwnerFollowersUrl];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.ownerIdentifier = [aDecoder decodeDoubleForKey:kACCOwnerId];
    self.organizationsUrl = [aDecoder decodeObjectForKey:kACCOwnerOrganizationsUrl];
    self.receivedEventsUrl = [aDecoder decodeObjectForKey:kACCOwnerReceivedEventsUrl];
    self.followingUrl = [aDecoder decodeObjectForKey:kACCOwnerFollowingUrl];
    self.login = [aDecoder decodeObjectForKey:kACCOwnerLogin];
    self.subscriptionsUrl = [aDecoder decodeObjectForKey:kACCOwnerSubscriptionsUrl];
    self.avatarUrl = [aDecoder decodeObjectForKey:kACCOwnerAvatarUrl];
    self.url = [aDecoder decodeObjectForKey:kACCOwnerUrl];
    self.type = [aDecoder decodeObjectForKey:kACCOwnerType];
    self.reposUrl = [aDecoder decodeObjectForKey:kACCOwnerReposUrl];
    self.htmlUrl = [aDecoder decodeObjectForKey:kACCOwnerHtmlUrl];
    self.eventsUrl = [aDecoder decodeObjectForKey:kACCOwnerEventsUrl];
    self.siteAdmin = [aDecoder decodeBoolForKey:kACCOwnerSiteAdmin];
    self.starredUrl = [aDecoder decodeObjectForKey:kACCOwnerStarredUrl];
    self.gistsUrl = [aDecoder decodeObjectForKey:kACCOwnerGistsUrl];
    self.gravatarId = [aDecoder decodeObjectForKey:kACCOwnerGravatarId];
    self.followersUrl = [aDecoder decodeObjectForKey:kACCOwnerFollowersUrl];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeDouble:_ownerIdentifier forKey:kACCOwnerId];
    [aCoder encodeObject:_organizationsUrl forKey:kACCOwnerOrganizationsUrl];
    [aCoder encodeObject:_receivedEventsUrl forKey:kACCOwnerReceivedEventsUrl];
    [aCoder encodeObject:_followingUrl forKey:kACCOwnerFollowingUrl];
    [aCoder encodeObject:_login forKey:kACCOwnerLogin];
    [aCoder encodeObject:_subscriptionsUrl forKey:kACCOwnerSubscriptionsUrl];
    [aCoder encodeObject:_avatarUrl forKey:kACCOwnerAvatarUrl];
    [aCoder encodeObject:_url forKey:kACCOwnerUrl];
    [aCoder encodeObject:_type forKey:kACCOwnerType];
    [aCoder encodeObject:_reposUrl forKey:kACCOwnerReposUrl];
    [aCoder encodeObject:_htmlUrl forKey:kACCOwnerHtmlUrl];
    [aCoder encodeObject:_eventsUrl forKey:kACCOwnerEventsUrl];
    [aCoder encodeBool:_siteAdmin forKey:kACCOwnerSiteAdmin];
    [aCoder encodeObject:_starredUrl forKey:kACCOwnerStarredUrl];
    [aCoder encodeObject:_gistsUrl forKey:kACCOwnerGistsUrl];
    [aCoder encodeObject:_gravatarId forKey:kACCOwnerGravatarId];
    [aCoder encodeObject:_followersUrl forKey:kACCOwnerFollowersUrl];
}

- (id)copyWithZone:(NSZone *)zone
{
    ACCOwner *copy = [[ACCOwner alloc] init];
    
    if (copy) {

        copy.ownerIdentifier = self.ownerIdentifier;
        copy.organizationsUrl = [self.organizationsUrl copyWithZone:zone];
        copy.receivedEventsUrl = [self.receivedEventsUrl copyWithZone:zone];
        copy.followingUrl = [self.followingUrl copyWithZone:zone];
        copy.login = [self.login copyWithZone:zone];
        copy.subscriptionsUrl = [self.subscriptionsUrl copyWithZone:zone];
        copy.avatarUrl = [self.avatarUrl copyWithZone:zone];
        copy.url = [self.url copyWithZone:zone];
        copy.type = [self.type copyWithZone:zone];
        copy.reposUrl = [self.reposUrl copyWithZone:zone];
        copy.htmlUrl = [self.htmlUrl copyWithZone:zone];
        copy.eventsUrl = [self.eventsUrl copyWithZone:zone];
        copy.siteAdmin = self.siteAdmin;
        copy.starredUrl = [self.starredUrl copyWithZone:zone];
        copy.gistsUrl = [self.gistsUrl copyWithZone:zone];
        copy.gravatarId = [self.gravatarId copyWithZone:zone];
        copy.followersUrl = [self.followersUrl copyWithZone:zone];
    }
    
    return copy;
}


@end
