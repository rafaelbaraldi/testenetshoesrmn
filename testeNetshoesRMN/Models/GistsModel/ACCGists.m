//
//  ACCGists.m
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ACCGists.h"
#import "ACCOwner.h"
#import "ACCFiles.h"


NSString *const kACCGistsId = @"id";
NSString *const kACCGistsComments = @"comments";
NSString *const kACCGistsDescription = @"description";
NSString *const kACCGistsCreatedAt = @"created_at";
NSString *const kACCGistsOwner = @"owner";
NSString *const kACCGistsUrl = @"url";
NSString *const kACCGistsGitPushUrl = @"git_push_url";
NSString *const kACCGistsFiles = @"files";
NSString *const kACCGistsHtmlUrl = @"html_url";
NSString *const kACCGistsGitPullUrl = @"git_pull_url";
NSString *const kACCGistsTruncated = @"truncated";
NSString *const kACCGistsUpdatedAt = @"updated_at";
NSString *const kACCGistsForksUrl = @"forks_url";
NSString *const kACCGistsCommitsUrl = @"commits_url";
NSString *const kACCGistsUser = @"user";
NSString *const kACCGistsPublic = @"public";
NSString *const kACCGistsCommentsUrl = @"comments_url";


@interface ACCGists ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ACCGists

@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize comments = _comments;
@synthesize internalBaseClassDescription = _internalBaseClassDescription;
@synthesize createdAt = _createdAt;
@synthesize owner = _owner;
@synthesize url = _url;
@synthesize gitPushUrl = _gitPushUrl;
@synthesize files = _files;
@synthesize htmlUrl = _htmlUrl;
@synthesize gitPullUrl = _gitPullUrl;
@synthesize truncated = _truncated;
@synthesize updatedAt = _updatedAt;
@synthesize forksUrl = _forksUrl;
@synthesize commitsUrl = _commitsUrl;
@synthesize user = _user;
@synthesize publicProperty = _publicProperty;
@synthesize commentsUrl = _commentsUrl;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.internalBaseClassIdentifier = [self objectOrNilForKey:kACCGistsId fromDictionary:dict];
            self.comments = [[self objectOrNilForKey:kACCGistsComments fromDictionary:dict] doubleValue];
            self.internalBaseClassDescription = [self objectOrNilForKey:kACCGistsDescription fromDictionary:dict];
            self.createdAt = [self objectOrNilForKey:kACCGistsCreatedAt fromDictionary:dict];
            self.owner = [ACCOwner modelObjectWithDictionary:[dict objectForKey:kACCGistsOwner]];
            self.url = [self objectOrNilForKey:kACCGistsUrl fromDictionary:dict];
            self.gitPushUrl = [self objectOrNilForKey:kACCGistsGitPushUrl fromDictionary:dict];
            self.files = [self objectOrNilForKey:kACCGistsFiles fromDictionary:dict];
            self.htmlUrl = [self objectOrNilForKey:kACCGistsHtmlUrl fromDictionary:dict];
            self.gitPullUrl = [self objectOrNilForKey:kACCGistsGitPullUrl fromDictionary:dict];
            self.truncated = [[self objectOrNilForKey:kACCGistsTruncated fromDictionary:dict] boolValue];
            self.updatedAt = [self objectOrNilForKey:kACCGistsUpdatedAt fromDictionary:dict];
            self.forksUrl = [self objectOrNilForKey:kACCGistsForksUrl fromDictionary:dict];
            self.commitsUrl = [self objectOrNilForKey:kACCGistsCommitsUrl fromDictionary:dict];
            self.user = [self objectOrNilForKey:kACCGistsUser fromDictionary:dict];
            self.publicProperty = [[self objectOrNilForKey:kACCGistsPublic fromDictionary:dict] boolValue];
            self.commentsUrl = [self objectOrNilForKey:kACCGistsCommentsUrl fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.internalBaseClassIdentifier forKey:kACCGistsId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.comments] forKey:kACCGistsComments];
    [mutableDict setValue:self.internalBaseClassDescription forKey:kACCGistsDescription];
    [mutableDict setValue:self.createdAt forKey:kACCGistsCreatedAt];
    [mutableDict setValue:[self.owner dictionaryRepresentation] forKey:kACCGistsOwner];
    [mutableDict setValue:self.url forKey:kACCGistsUrl];
    [mutableDict setValue:self.gitPushUrl forKey:kACCGistsGitPushUrl];
    [mutableDict setValue:self.files forKey:kACCGistsFiles];
    
//    NSMutableArray *tempArrayForFiles = [NSMutableArray array];
//    for (NSObject *subArrayObject in self.files) {
//        if([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
//            // This class is a model object
//            [tempArrayForFiles addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
//        } else {
//            // Generic object
//            [tempArrayForFiles addObject:subArrayObject];
//        }
//    }
//    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForFiles] forKey:kACCGistsFiles];
    
    [mutableDict setValue:self.htmlUrl forKey:kACCGistsHtmlUrl];
    [mutableDict setValue:self.gitPullUrl forKey:kACCGistsGitPullUrl];
    [mutableDict setValue:[NSNumber numberWithBool:self.truncated] forKey:kACCGistsTruncated];
    [mutableDict setValue:self.updatedAt forKey:kACCGistsUpdatedAt];
    [mutableDict setValue:self.forksUrl forKey:kACCGistsForksUrl];
    [mutableDict setValue:self.commitsUrl forKey:kACCGistsCommitsUrl];
    [mutableDict setValue:self.user forKey:kACCGistsUser];
    [mutableDict setValue:[NSNumber numberWithBool:self.publicProperty] forKey:kACCGistsPublic];
    [mutableDict setValue:self.commentsUrl forKey:kACCGistsCommentsUrl];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.internalBaseClassIdentifier = [aDecoder decodeObjectForKey:kACCGistsId];
    self.comments = [aDecoder decodeDoubleForKey:kACCGistsComments];
    self.internalBaseClassDescription = [aDecoder decodeObjectForKey:kACCGistsDescription];
    self.createdAt = [aDecoder decodeObjectForKey:kACCGistsCreatedAt];
    self.owner = [aDecoder decodeObjectForKey:kACCGistsOwner];
    self.url = [aDecoder decodeObjectForKey:kACCGistsUrl];
    self.gitPushUrl = [aDecoder decodeObjectForKey:kACCGistsGitPushUrl];
    self.files = [aDecoder decodeObjectForKey:kACCGistsFiles];
    self.htmlUrl = [aDecoder decodeObjectForKey:kACCGistsHtmlUrl];
    self.gitPullUrl = [aDecoder decodeObjectForKey:kACCGistsGitPullUrl];
    self.truncated = [aDecoder decodeBoolForKey:kACCGistsTruncated];
    self.updatedAt = [aDecoder decodeObjectForKey:kACCGistsUpdatedAt];
    self.forksUrl = [aDecoder decodeObjectForKey:kACCGistsForksUrl];
    self.commitsUrl = [aDecoder decodeObjectForKey:kACCGistsCommitsUrl];
    self.user = [aDecoder decodeObjectForKey:kACCGistsUser];
    self.publicProperty = [aDecoder decodeBoolForKey:kACCGistsPublic];
    self.commentsUrl = [aDecoder decodeObjectForKey:kACCGistsCommentsUrl];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_internalBaseClassIdentifier forKey:kACCGistsId];
    [aCoder encodeDouble:_comments forKey:kACCGistsComments];
    [aCoder encodeObject:_internalBaseClassDescription forKey:kACCGistsDescription];
    [aCoder encodeObject:_createdAt forKey:kACCGistsCreatedAt];
    [aCoder encodeObject:_owner forKey:kACCGistsOwner];
    [aCoder encodeObject:_url forKey:kACCGistsUrl];
    [aCoder encodeObject:_gitPushUrl forKey:kACCGistsGitPushUrl];
    [aCoder encodeObject:_files forKey:kACCGistsFiles];
    [aCoder encodeObject:_htmlUrl forKey:kACCGistsHtmlUrl];
    [aCoder encodeObject:_gitPullUrl forKey:kACCGistsGitPullUrl];
    [aCoder encodeBool:_truncated forKey:kACCGistsTruncated];
    [aCoder encodeObject:_updatedAt forKey:kACCGistsUpdatedAt];
    [aCoder encodeObject:_forksUrl forKey:kACCGistsForksUrl];
    [aCoder encodeObject:_commitsUrl forKey:kACCGistsCommitsUrl];
    [aCoder encodeObject:_user forKey:kACCGistsUser];
    [aCoder encodeBool:_publicProperty forKey:kACCGistsPublic];
    [aCoder encodeObject:_commentsUrl forKey:kACCGistsCommentsUrl];
}

- (id)copyWithZone:(NSZone *)zone
{
    ACCGists *copy = [[ACCGists alloc] init];
    
    if (copy) {

        copy.internalBaseClassIdentifier = [self.internalBaseClassIdentifier copyWithZone:zone];
        copy.comments = self.comments;
        copy.internalBaseClassDescription = [self.internalBaseClassDescription copyWithZone:zone];
        copy.createdAt = [self.createdAt copyWithZone:zone];
        copy.owner = [self.owner copyWithZone:zone];
        copy.url = [self.url copyWithZone:zone];
        copy.gitPushUrl = [self.gitPushUrl copyWithZone:zone];
        copy.files = [self.files copyWithZone:zone];
        copy.htmlUrl = [self.htmlUrl copyWithZone:zone];
        copy.gitPullUrl = [self.gitPullUrl copyWithZone:zone];
        copy.truncated = self.truncated;
        copy.updatedAt = [self.updatedAt copyWithZone:zone];
        copy.forksUrl = [self.forksUrl copyWithZone:zone];
        copy.commitsUrl = [self.commitsUrl copyWithZone:zone];
        copy.user = [self.user copyWithZone:zone];
        copy.publicProperty = self.publicProperty;
        copy.commentsUrl = [self.commentsUrl copyWithZone:zone];
    }
    
    return copy;
}


@end
