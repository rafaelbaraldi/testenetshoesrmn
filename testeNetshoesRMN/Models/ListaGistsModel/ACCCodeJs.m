//
//  ACCCodeJs.m
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ACCCodeJs.h"


NSString *const kACCCodeJsRawUrl = @"raw_url";
NSString *const kACCCodeJsLanguage = @"language";
NSString *const kACCCodeJsFilename = @"filename";
NSString *const kACCCodeJsSize = @"size";
NSString *const kACCCodeJsType = @"type";


@interface ACCCodeJs ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ACCCodeJs

@synthesize rawUrl = _rawUrl;
@synthesize language = _language;
@synthesize filename = _filename;
@synthesize size = _size;
@synthesize type = _type;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.rawUrl = [self objectOrNilForKey:kACCCodeJsRawUrl fromDictionary:dict];
            self.language = [self objectOrNilForKey:kACCCodeJsLanguage fromDictionary:dict];
            self.filename = [self objectOrNilForKey:kACCCodeJsFilename fromDictionary:dict];
            self.size = [[self objectOrNilForKey:kACCCodeJsSize fromDictionary:dict] doubleValue];
            self.type = [self objectOrNilForKey:kACCCodeJsType fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.rawUrl forKey:kACCCodeJsRawUrl];
    [mutableDict setValue:self.language forKey:kACCCodeJsLanguage];
    [mutableDict setValue:self.filename forKey:kACCCodeJsFilename];
    [mutableDict setValue:[NSNumber numberWithDouble:self.size] forKey:kACCCodeJsSize];
    [mutableDict setValue:self.type forKey:kACCCodeJsType];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.rawUrl = [aDecoder decodeObjectForKey:kACCCodeJsRawUrl];
    self.language = [aDecoder decodeObjectForKey:kACCCodeJsLanguage];
    self.filename = [aDecoder decodeObjectForKey:kACCCodeJsFilename];
    self.size = [aDecoder decodeDoubleForKey:kACCCodeJsSize];
    self.type = [aDecoder decodeObjectForKey:kACCCodeJsType];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_rawUrl forKey:kACCCodeJsRawUrl];
    [aCoder encodeObject:_language forKey:kACCCodeJsLanguage];
    [aCoder encodeObject:_filename forKey:kACCCodeJsFilename];
    [aCoder encodeDouble:_size forKey:kACCCodeJsSize];
    [aCoder encodeObject:_type forKey:kACCCodeJsType];
}

- (id)copyWithZone:(NSZone *)zone
{
    ACCCodeJs *copy = [[ACCCodeJs alloc] init];
    
    if (copy) {

        copy.rawUrl = [self.rawUrl copyWithZone:zone];
        copy.language = [self.language copyWithZone:zone];
        copy.filename = [self.filename copyWithZone:zone];
        copy.size = self.size;
        copy.type = [self.type copyWithZone:zone];
    }
    
    return copy;
}


@end
