//
//  ACCIndexHtml.m
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ACCIndexHtml.h"


NSString *const kACCIndexHtmlRawUrl = @"raw_url";
NSString *const kACCIndexHtmlLanguage = @"language";
NSString *const kACCIndexHtmlFilename = @"filename";
NSString *const kACCIndexHtmlSize = @"size";
NSString *const kACCIndexHtmlType = @"type";


@interface ACCIndexHtml ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ACCIndexHtml

@synthesize rawUrl = _rawUrl;
@synthesize language = _language;
@synthesize filename = _filename;
@synthesize size = _size;
@synthesize type = _type;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.rawUrl = [self objectOrNilForKey:kACCIndexHtmlRawUrl fromDictionary:dict];
            self.language = [self objectOrNilForKey:kACCIndexHtmlLanguage fromDictionary:dict];
            self.filename = [self objectOrNilForKey:kACCIndexHtmlFilename fromDictionary:dict];
            self.size = [[self objectOrNilForKey:kACCIndexHtmlSize fromDictionary:dict] doubleValue];
            self.type = [self objectOrNilForKey:kACCIndexHtmlType fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.rawUrl forKey:kACCIndexHtmlRawUrl];
    [mutableDict setValue:self.language forKey:kACCIndexHtmlLanguage];
    [mutableDict setValue:self.filename forKey:kACCIndexHtmlFilename];
    [mutableDict setValue:[NSNumber numberWithDouble:self.size] forKey:kACCIndexHtmlSize];
    [mutableDict setValue:self.type forKey:kACCIndexHtmlType];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.rawUrl = [aDecoder decodeObjectForKey:kACCIndexHtmlRawUrl];
    self.language = [aDecoder decodeObjectForKey:kACCIndexHtmlLanguage];
    self.filename = [aDecoder decodeObjectForKey:kACCIndexHtmlFilename];
    self.size = [aDecoder decodeDoubleForKey:kACCIndexHtmlSize];
    self.type = [aDecoder decodeObjectForKey:kACCIndexHtmlType];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_rawUrl forKey:kACCIndexHtmlRawUrl];
    [aCoder encodeObject:_language forKey:kACCIndexHtmlLanguage];
    [aCoder encodeObject:_filename forKey:kACCIndexHtmlFilename];
    [aCoder encodeDouble:_size forKey:kACCIndexHtmlSize];
    [aCoder encodeObject:_type forKey:kACCIndexHtmlType];
}

- (id)copyWithZone:(NSZone *)zone
{
    ACCIndexHtml *copy = [[ACCIndexHtml alloc] init];
    
    if (copy) {

        copy.rawUrl = [self.rawUrl copyWithZone:zone];
        copy.language = [self.language copyWithZone:zone];
        copy.filename = [self.filename copyWithZone:zone];
        copy.size = self.size;
        copy.type = [self.type copyWithZone:zone];
    }
    
    return copy;
}


@end
