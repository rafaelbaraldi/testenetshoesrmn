//
//  ACCFiles.h
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ACCKwargsOnDecoratedFuncsIpynb, ACCOpenpanzerSaveJson, ACCConfigJson, ACCRELKSh, ACCUninstallTxt, ACCInspectUriClassPropertiesCs, ACCMyCss, ACCUntrustedLvl11SolutionJs, ACCAZzJPLMarkdown, ACCJavaMergeTwoArray, ACCWurstClientV2212ErrorReportMd, ACCGistfile1Txt, ACCCodingReferenceSheet, ACCStyles, ACC1RegistrySnapshotXml, ACCSummaryMd, ACCUntrustedLvl1SolutionJs, ACCTributePageMarkdown, ACCInstallTxt, ACCUntrustedLvl14SolutionJs, ACCReflectPy, ACCDataJs, ACCCodeJs, ACCFilesSnapshotXml, ACCMicroProcessEngine, ACCIndexHtml;

@interface ACCFiles : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) ACCKwargsOnDecoratedFuncsIpynb *kwargsOnDecoratedFuncsIpynb;
@property (nonatomic, strong) ACCOpenpanzerSaveJson *openpanzerSaveJson;
@property (nonatomic, strong) ACCConfigJson *configJson;
@property (nonatomic, strong) ACCRELKSh *rELKSh;
@property (nonatomic, strong) ACCUninstallTxt *uninstallTxt;
@property (nonatomic, strong) ACCInspectUriClassPropertiesCs *inspectUriClassPropertiesCs;
@property (nonatomic, strong) ACCMyCss *myCss;
@property (nonatomic, strong) ACCUntrustedLvl11SolutionJs *untrustedLvl11SolutionJs;
@property (nonatomic, strong) ACCAZzJPLMarkdown *aZzJPLMarkdown;
@property (nonatomic, strong) ACCJavaMergeTwoArray *javaMergeTwoArray;
@property (nonatomic, strong) ACCWurstClientV2212ErrorReportMd *wurstClientV2212ErrorReportMd;
@property (nonatomic, strong) ACCGistfile1Txt *gistfile1Txt;
@property (nonatomic, strong) ACCCodingReferenceSheet *codingReferenceSheet;
@property (nonatomic, strong) ACCStyles *styles;
@property (nonatomic, strong) ACC1RegistrySnapshotXml * x1RegistrySnapshotXml;
@property (nonatomic, strong) ACCSummaryMd *summaryMd;
@property (nonatomic, strong) ACCUntrustedLvl1SolutionJs *untrustedLvl1SolutionJs;
@property (nonatomic, strong) ACCTributePageMarkdown *tributePageMarkdown;
@property (nonatomic, strong) ACCInstallTxt *installTxt;
@property (nonatomic, strong) ACCUntrustedLvl14SolutionJs *untrustedLvl14SolutionJs;
@property (nonatomic, strong) ACCReflectPy *reflectPy;
@property (nonatomic, strong) ACCDataJs *dataJs;
@property (nonatomic, strong) ACCCodeJs *codeJs;
@property (nonatomic, strong) ACCFilesSnapshotXml *filesSnapshotXml;
@property (nonatomic, strong) ACCMicroProcessEngine *microProcessEngine;
@property (nonatomic, strong) ACCIndexHtml *indexHtml;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
