//
//  ACCCodingReferenceSheet.m
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ACCCodingReferenceSheet.h"


NSString *const kACCCodingReferenceSheetRawUrl = @"raw_url";
NSString *const kACCCodingReferenceSheetLanguage = @"language";
NSString *const kACCCodingReferenceSheetFilename = @"filename";
NSString *const kACCCodingReferenceSheetSize = @"size";
NSString *const kACCCodingReferenceSheetType = @"type";


@interface ACCCodingReferenceSheet ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ACCCodingReferenceSheet

@synthesize rawUrl = _rawUrl;
@synthesize language = _language;
@synthesize filename = _filename;
@synthesize size = _size;
@synthesize type = _type;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.rawUrl = [self objectOrNilForKey:kACCCodingReferenceSheetRawUrl fromDictionary:dict];
            self.language = [self objectOrNilForKey:kACCCodingReferenceSheetLanguage fromDictionary:dict];
            self.filename = [self objectOrNilForKey:kACCCodingReferenceSheetFilename fromDictionary:dict];
            self.size = [[self objectOrNilForKey:kACCCodingReferenceSheetSize fromDictionary:dict] doubleValue];
            self.type = [self objectOrNilForKey:kACCCodingReferenceSheetType fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.rawUrl forKey:kACCCodingReferenceSheetRawUrl];
    [mutableDict setValue:self.language forKey:kACCCodingReferenceSheetLanguage];
    [mutableDict setValue:self.filename forKey:kACCCodingReferenceSheetFilename];
    [mutableDict setValue:[NSNumber numberWithDouble:self.size] forKey:kACCCodingReferenceSheetSize];
    [mutableDict setValue:self.type forKey:kACCCodingReferenceSheetType];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.rawUrl = [aDecoder decodeObjectForKey:kACCCodingReferenceSheetRawUrl];
    self.language = [aDecoder decodeObjectForKey:kACCCodingReferenceSheetLanguage];
    self.filename = [aDecoder decodeObjectForKey:kACCCodingReferenceSheetFilename];
    self.size = [aDecoder decodeDoubleForKey:kACCCodingReferenceSheetSize];
    self.type = [aDecoder decodeObjectForKey:kACCCodingReferenceSheetType];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_rawUrl forKey:kACCCodingReferenceSheetRawUrl];
    [aCoder encodeObject:_language forKey:kACCCodingReferenceSheetLanguage];
    [aCoder encodeObject:_filename forKey:kACCCodingReferenceSheetFilename];
    [aCoder encodeDouble:_size forKey:kACCCodingReferenceSheetSize];
    [aCoder encodeObject:_type forKey:kACCCodingReferenceSheetType];
}

- (id)copyWithZone:(NSZone *)zone
{
    ACCCodingReferenceSheet *copy = [[ACCCodingReferenceSheet alloc] init];
    
    if (copy) {

        copy.rawUrl = [self.rawUrl copyWithZone:zone];
        copy.language = [self.language copyWithZone:zone];
        copy.filename = [self.filename copyWithZone:zone];
        copy.size = self.size;
        copy.type = [self.type copyWithZone:zone];
    }
    
    return copy;
}


@end
