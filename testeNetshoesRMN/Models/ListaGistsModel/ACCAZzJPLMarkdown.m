//
//  ACCAZzJPLMarkdown.m
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ACCAZzJPLMarkdown.h"


NSString *const kACCAZzJPLMarkdownRawUrl = @"raw_url";
NSString *const kACCAZzJPLMarkdownLanguage = @"language";
NSString *const kACCAZzJPLMarkdownFilename = @"filename";
NSString *const kACCAZzJPLMarkdownSize = @"size";
NSString *const kACCAZzJPLMarkdownType = @"type";


@interface ACCAZzJPLMarkdown ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ACCAZzJPLMarkdown

@synthesize rawUrl = _rawUrl;
@synthesize language = _language;
@synthesize filename = _filename;
@synthesize size = _size;
@synthesize type = _type;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.rawUrl = [self objectOrNilForKey:kACCAZzJPLMarkdownRawUrl fromDictionary:dict];
            self.language = [self objectOrNilForKey:kACCAZzJPLMarkdownLanguage fromDictionary:dict];
            self.filename = [self objectOrNilForKey:kACCAZzJPLMarkdownFilename fromDictionary:dict];
            self.size = [[self objectOrNilForKey:kACCAZzJPLMarkdownSize fromDictionary:dict] doubleValue];
            self.type = [self objectOrNilForKey:kACCAZzJPLMarkdownType fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.rawUrl forKey:kACCAZzJPLMarkdownRawUrl];
    [mutableDict setValue:self.language forKey:kACCAZzJPLMarkdownLanguage];
    [mutableDict setValue:self.filename forKey:kACCAZzJPLMarkdownFilename];
    [mutableDict setValue:[NSNumber numberWithDouble:self.size] forKey:kACCAZzJPLMarkdownSize];
    [mutableDict setValue:self.type forKey:kACCAZzJPLMarkdownType];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.rawUrl = [aDecoder decodeObjectForKey:kACCAZzJPLMarkdownRawUrl];
    self.language = [aDecoder decodeObjectForKey:kACCAZzJPLMarkdownLanguage];
    self.filename = [aDecoder decodeObjectForKey:kACCAZzJPLMarkdownFilename];
    self.size = [aDecoder decodeDoubleForKey:kACCAZzJPLMarkdownSize];
    self.type = [aDecoder decodeObjectForKey:kACCAZzJPLMarkdownType];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_rawUrl forKey:kACCAZzJPLMarkdownRawUrl];
    [aCoder encodeObject:_language forKey:kACCAZzJPLMarkdownLanguage];
    [aCoder encodeObject:_filename forKey:kACCAZzJPLMarkdownFilename];
    [aCoder encodeDouble:_size forKey:kACCAZzJPLMarkdownSize];
    [aCoder encodeObject:_type forKey:kACCAZzJPLMarkdownType];
}

- (id)copyWithZone:(NSZone *)zone
{
    ACCAZzJPLMarkdown *copy = [[ACCAZzJPLMarkdown alloc] init];
    
    if (copy) {

        copy.rawUrl = [self.rawUrl copyWithZone:zone];
        copy.language = [self.language copyWithZone:zone];
        copy.filename = [self.filename copyWithZone:zone];
        copy.size = self.size;
        copy.type = [self.type copyWithZone:zone];
    }
    
    return copy;
}


@end
