//
//  ACCSummaryMd.m
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ACCSummaryMd.h"


NSString *const kACCSummaryMdRawUrl = @"raw_url";
NSString *const kACCSummaryMdLanguage = @"language";
NSString *const kACCSummaryMdFilename = @"filename";
NSString *const kACCSummaryMdSize = @"size";
NSString *const kACCSummaryMdType = @"type";


@interface ACCSummaryMd ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ACCSummaryMd

@synthesize rawUrl = _rawUrl;
@synthesize language = _language;
@synthesize filename = _filename;
@synthesize size = _size;
@synthesize type = _type;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.rawUrl = [self objectOrNilForKey:kACCSummaryMdRawUrl fromDictionary:dict];
            self.language = [self objectOrNilForKey:kACCSummaryMdLanguage fromDictionary:dict];
            self.filename = [self objectOrNilForKey:kACCSummaryMdFilename fromDictionary:dict];
            self.size = [[self objectOrNilForKey:kACCSummaryMdSize fromDictionary:dict] doubleValue];
            self.type = [self objectOrNilForKey:kACCSummaryMdType fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.rawUrl forKey:kACCSummaryMdRawUrl];
    [mutableDict setValue:self.language forKey:kACCSummaryMdLanguage];
    [mutableDict setValue:self.filename forKey:kACCSummaryMdFilename];
    [mutableDict setValue:[NSNumber numberWithDouble:self.size] forKey:kACCSummaryMdSize];
    [mutableDict setValue:self.type forKey:kACCSummaryMdType];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.rawUrl = [aDecoder decodeObjectForKey:kACCSummaryMdRawUrl];
    self.language = [aDecoder decodeObjectForKey:kACCSummaryMdLanguage];
    self.filename = [aDecoder decodeObjectForKey:kACCSummaryMdFilename];
    self.size = [aDecoder decodeDoubleForKey:kACCSummaryMdSize];
    self.type = [aDecoder decodeObjectForKey:kACCSummaryMdType];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_rawUrl forKey:kACCSummaryMdRawUrl];
    [aCoder encodeObject:_language forKey:kACCSummaryMdLanguage];
    [aCoder encodeObject:_filename forKey:kACCSummaryMdFilename];
    [aCoder encodeDouble:_size forKey:kACCSummaryMdSize];
    [aCoder encodeObject:_type forKey:kACCSummaryMdType];
}

- (id)copyWithZone:(NSZone *)zone
{
    ACCSummaryMd *copy = [[ACCSummaryMd alloc] init];
    
    if (copy) {

        copy.rawUrl = [self.rawUrl copyWithZone:zone];
        copy.language = [self.language copyWithZone:zone];
        copy.filename = [self.filename copyWithZone:zone];
        copy.size = self.size;
        copy.type = [self.type copyWithZone:zone];
    }
    
    return copy;
}


@end
