//
//  ACCMicroProcessEngine.m
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ACCMicroProcessEngine.h"


NSString *const kACCMicroProcessEngineRawUrl = @"raw_url";
NSString *const kACCMicroProcessEngineLanguage = @"language";
NSString *const kACCMicroProcessEngineFilename = @"filename";
NSString *const kACCMicroProcessEngineSize = @"size";
NSString *const kACCMicroProcessEngineType = @"type";


@interface ACCMicroProcessEngine ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ACCMicroProcessEngine

@synthesize rawUrl = _rawUrl;
@synthesize language = _language;
@synthesize filename = _filename;
@synthesize size = _size;
@synthesize type = _type;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.rawUrl = [self objectOrNilForKey:kACCMicroProcessEngineRawUrl fromDictionary:dict];
            self.language = [self objectOrNilForKey:kACCMicroProcessEngineLanguage fromDictionary:dict];
            self.filename = [self objectOrNilForKey:kACCMicroProcessEngineFilename fromDictionary:dict];
            self.size = [[self objectOrNilForKey:kACCMicroProcessEngineSize fromDictionary:dict] doubleValue];
            self.type = [self objectOrNilForKey:kACCMicroProcessEngineType fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.rawUrl forKey:kACCMicroProcessEngineRawUrl];
    [mutableDict setValue:self.language forKey:kACCMicroProcessEngineLanguage];
    [mutableDict setValue:self.filename forKey:kACCMicroProcessEngineFilename];
    [mutableDict setValue:[NSNumber numberWithDouble:self.size] forKey:kACCMicroProcessEngineSize];
    [mutableDict setValue:self.type forKey:kACCMicroProcessEngineType];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.rawUrl = [aDecoder decodeObjectForKey:kACCMicroProcessEngineRawUrl];
    self.language = [aDecoder decodeObjectForKey:kACCMicroProcessEngineLanguage];
    self.filename = [aDecoder decodeObjectForKey:kACCMicroProcessEngineFilename];
    self.size = [aDecoder decodeDoubleForKey:kACCMicroProcessEngineSize];
    self.type = [aDecoder decodeObjectForKey:kACCMicroProcessEngineType];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_rawUrl forKey:kACCMicroProcessEngineRawUrl];
    [aCoder encodeObject:_language forKey:kACCMicroProcessEngineLanguage];
    [aCoder encodeObject:_filename forKey:kACCMicroProcessEngineFilename];
    [aCoder encodeDouble:_size forKey:kACCMicroProcessEngineSize];
    [aCoder encodeObject:_type forKey:kACCMicroProcessEngineType];
}

- (id)copyWithZone:(NSZone *)zone
{
    ACCMicroProcessEngine *copy = [[ACCMicroProcessEngine alloc] init];
    
    if (copy) {

        copy.rawUrl = [self.rawUrl copyWithZone:zone];
        copy.language = [self.language copyWithZone:zone];
        copy.filename = [self.filename copyWithZone:zone];
        copy.size = self.size;
        copy.type = [self.type copyWithZone:zone];
    }
    
    return copy;
}


@end
