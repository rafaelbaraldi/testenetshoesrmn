//
//  ACCUntrustedLvl1SolutionJs.m
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ACCUntrustedLvl1SolutionJs.h"


NSString *const kACCUntrustedLvl1SolutionJsRawUrl = @"raw_url";
NSString *const kACCUntrustedLvl1SolutionJsLanguage = @"language";
NSString *const kACCUntrustedLvl1SolutionJsFilename = @"filename";
NSString *const kACCUntrustedLvl1SolutionJsSize = @"size";
NSString *const kACCUntrustedLvl1SolutionJsType = @"type";


@interface ACCUntrustedLvl1SolutionJs ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ACCUntrustedLvl1SolutionJs

@synthesize rawUrl = _rawUrl;
@synthesize language = _language;
@synthesize filename = _filename;
@synthesize size = _size;
@synthesize type = _type;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.rawUrl = [self objectOrNilForKey:kACCUntrustedLvl1SolutionJsRawUrl fromDictionary:dict];
            self.language = [self objectOrNilForKey:kACCUntrustedLvl1SolutionJsLanguage fromDictionary:dict];
            self.filename = [self objectOrNilForKey:kACCUntrustedLvl1SolutionJsFilename fromDictionary:dict];
            self.size = [[self objectOrNilForKey:kACCUntrustedLvl1SolutionJsSize fromDictionary:dict] doubleValue];
            self.type = [self objectOrNilForKey:kACCUntrustedLvl1SolutionJsType fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.rawUrl forKey:kACCUntrustedLvl1SolutionJsRawUrl];
    [mutableDict setValue:self.language forKey:kACCUntrustedLvl1SolutionJsLanguage];
    [mutableDict setValue:self.filename forKey:kACCUntrustedLvl1SolutionJsFilename];
    [mutableDict setValue:[NSNumber numberWithDouble:self.size] forKey:kACCUntrustedLvl1SolutionJsSize];
    [mutableDict setValue:self.type forKey:kACCUntrustedLvl1SolutionJsType];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.rawUrl = [aDecoder decodeObjectForKey:kACCUntrustedLvl1SolutionJsRawUrl];
    self.language = [aDecoder decodeObjectForKey:kACCUntrustedLvl1SolutionJsLanguage];
    self.filename = [aDecoder decodeObjectForKey:kACCUntrustedLvl1SolutionJsFilename];
    self.size = [aDecoder decodeDoubleForKey:kACCUntrustedLvl1SolutionJsSize];
    self.type = [aDecoder decodeObjectForKey:kACCUntrustedLvl1SolutionJsType];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_rawUrl forKey:kACCUntrustedLvl1SolutionJsRawUrl];
    [aCoder encodeObject:_language forKey:kACCUntrustedLvl1SolutionJsLanguage];
    [aCoder encodeObject:_filename forKey:kACCUntrustedLvl1SolutionJsFilename];
    [aCoder encodeDouble:_size forKey:kACCUntrustedLvl1SolutionJsSize];
    [aCoder encodeObject:_type forKey:kACCUntrustedLvl1SolutionJsType];
}

- (id)copyWithZone:(NSZone *)zone
{
    ACCUntrustedLvl1SolutionJs *copy = [[ACCUntrustedLvl1SolutionJs alloc] init];
    
    if (copy) {

        copy.rawUrl = [self.rawUrl copyWithZone:zone];
        copy.language = [self.language copyWithZone:zone];
        copy.filename = [self.filename copyWithZone:zone];
        copy.size = self.size;
        copy.type = [self.type copyWithZone:zone];
    }
    
    return copy;
}


@end
