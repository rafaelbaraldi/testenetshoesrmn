//
//  ACClistaGists.h
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ACCOwner, ACCFiles;

@interface ACClistaGists : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *internalBaseClassIdentifier;
@property (nonatomic, assign) double comments;
@property (nonatomic, strong) NSString *internalBaseClassDescription;
@property (nonatomic, strong) NSString *createdAt;
@property (nonatomic, strong) ACCOwner *owner;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *gitPushUrl;
@property (nonatomic, strong) ACCFiles *files;
@property (nonatomic, strong) NSString *htmlUrl;
@property (nonatomic, strong) NSString *gitPullUrl;
@property (nonatomic, assign) BOOL truncated;
@property (nonatomic, strong) NSString *updatedAt;
@property (nonatomic, strong) NSString *forksUrl;
@property (nonatomic, strong) NSString *commitsUrl;
@property (nonatomic, assign) id user;
@property (nonatomic, assign) BOOL publicProperty;
@property (nonatomic, strong) NSString *commentsUrl;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
