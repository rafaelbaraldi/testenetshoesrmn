//
//  ACCReflectPy.m
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ACCReflectPy.h"


NSString *const kACCReflectPyRawUrl = @"raw_url";
NSString *const kACCReflectPyLanguage = @"language";
NSString *const kACCReflectPyFilename = @"filename";
NSString *const kACCReflectPySize = @"size";
NSString *const kACCReflectPyType = @"type";


@interface ACCReflectPy ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ACCReflectPy

@synthesize rawUrl = _rawUrl;
@synthesize language = _language;
@synthesize filename = _filename;
@synthesize size = _size;
@synthesize type = _type;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.rawUrl = [self objectOrNilForKey:kACCReflectPyRawUrl fromDictionary:dict];
            self.language = [self objectOrNilForKey:kACCReflectPyLanguage fromDictionary:dict];
            self.filename = [self objectOrNilForKey:kACCReflectPyFilename fromDictionary:dict];
            self.size = [[self objectOrNilForKey:kACCReflectPySize fromDictionary:dict] doubleValue];
            self.type = [self objectOrNilForKey:kACCReflectPyType fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.rawUrl forKey:kACCReflectPyRawUrl];
    [mutableDict setValue:self.language forKey:kACCReflectPyLanguage];
    [mutableDict setValue:self.filename forKey:kACCReflectPyFilename];
    [mutableDict setValue:[NSNumber numberWithDouble:self.size] forKey:kACCReflectPySize];
    [mutableDict setValue:self.type forKey:kACCReflectPyType];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.rawUrl = [aDecoder decodeObjectForKey:kACCReflectPyRawUrl];
    self.language = [aDecoder decodeObjectForKey:kACCReflectPyLanguage];
    self.filename = [aDecoder decodeObjectForKey:kACCReflectPyFilename];
    self.size = [aDecoder decodeDoubleForKey:kACCReflectPySize];
    self.type = [aDecoder decodeObjectForKey:kACCReflectPyType];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_rawUrl forKey:kACCReflectPyRawUrl];
    [aCoder encodeObject:_language forKey:kACCReflectPyLanguage];
    [aCoder encodeObject:_filename forKey:kACCReflectPyFilename];
    [aCoder encodeDouble:_size forKey:kACCReflectPySize];
    [aCoder encodeObject:_type forKey:kACCReflectPyType];
}

- (id)copyWithZone:(NSZone *)zone
{
    ACCReflectPy *copy = [[ACCReflectPy alloc] init];
    
    if (copy) {

        copy.rawUrl = [self.rawUrl copyWithZone:zone];
        copy.language = [self.language copyWithZone:zone];
        copy.filename = [self.filename copyWithZone:zone];
        copy.size = self.size;
        copy.type = [self.type copyWithZone:zone];
    }
    
    return copy;
}


@end
