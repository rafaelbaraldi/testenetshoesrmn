//
//  ACCFiles.m
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ACCFiles.h"
#import "ACCKwargsOnDecoratedFuncsIpynb.h"
#import "ACCOpenpanzerSaveJson.h"
#import "ACCConfigJson.h"
#import "ACCRELKSh.h"
#import "ACCUninstallTxt.h"
#import "ACCInspectUriClassPropertiesCs.h"
#import "ACCMyCss.h"
#import "ACCUntrustedLvl11SolutionJs.h"
#import "ACCAZzJPLMarkdown.h"
#import "ACCJavaMergeTwoArray.h"
#import "ACCWurstClientV2212ErrorReportMd.h"
#import "ACCGistfile1Txt.h"
#import "ACCCodingReferenceSheet.h"
#import "ACCStyles.h"
#import "ACC1RegistrySnapshotXml.h"
#import "ACCSummaryMd.h"
#import "ACCUntrustedLvl1SolutionJs.h"
#import "ACCTributePageMarkdown.h"
#import "ACCInstallTxt.h"
#import "ACCUntrustedLvl14SolutionJs.h"
#import "ACCReflectPy.h"
#import "ACCDataJs.h"
#import "ACCCodeJs.h"
#import "ACCFilesSnapshotXml.h"
#import "ACCMicroProcessEngine.h"
#import "ACCIndexHtml.h"


NSString *const kACCFilesKwargsOnDecoratedFuncsIpynb = @"kwargs on decorated funcs.ipynb";
NSString *const kACCFilesOpenpanzerSaveJson = @"openpanzer-save.json";
NSString *const kACCFilesConfigJson = @"config.json";
NSString *const kACCFilesRELKSh = @"RELK.sh";
NSString *const kACCFilesUninstallTxt = @"Uninstall.txt";
NSString *const kACCFilesInspectUriClassPropertiesCs = @"InspectUriClassProperties.cs";
NSString *const kACCFilesMyCss = @"my.css";
NSString *const kACCFilesUntrustedLvl11SolutionJs = @"untrusted-lvl11-solution.js";
NSString *const kACCFilesAZzJPLMarkdown = @"aZzJPL.markdown";
NSString *const kACCFilesJavaMergeTwoArray = @"Java merge two array";
NSString *const kACCFilesWurstClientV2212ErrorReportMd = @"Wurst-Client-v2.21.2-Error-Report.md";
NSString *const kACCFilesGistfile1Txt = @"gistfile1.txt";
NSString *const kACCFilesCodingReferenceSheet = @"Coding Reference Sheet";
NSString *const kACCFilesStyles = @"styles";
NSString *const kACCFiles1RegistrySnapshotXml = @"1.RegistrySnapshot.xml";
NSString *const kACCFilesSummaryMd = @"_Summary.md";
NSString *const kACCFilesUntrustedLvl1SolutionJs = @"untrusted-lvl1-solution.js";
NSString *const kACCFilesTributePageMarkdown = @"Tribute Page.markdown";
NSString *const kACCFilesInstallTxt = @"Install.txt";
NSString *const kACCFilesUntrustedLvl14SolutionJs = @"untrusted-lvl14-solution.js";
NSString *const kACCFilesReflectPy = @"reflect.py";
NSString *const kACCFilesDataJs = @"data.js";
NSString *const kACCFilesCodeJs = @"code.js";
NSString *const kACCFilesFilesSnapshotXml = @"FilesSnapshot.xml";
NSString *const kACCFilesMicroProcessEngine = @"micro-process-engine";
NSString *const kACCFilesIndexHtml = @"index.html";


@interface ACCFiles ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ACCFiles

@synthesize kwargsOnDecoratedFuncsIpynb = _kwargsOnDecoratedFuncsIpynb;
@synthesize openpanzerSaveJson = _openpanzerSaveJson;
@synthesize configJson = _configJson;
@synthesize rELKSh = _rELKSh;
@synthesize uninstallTxt = _uninstallTxt;
@synthesize inspectUriClassPropertiesCs = _inspectUriClassPropertiesCs;
@synthesize myCss = _myCss;
@synthesize untrustedLvl11SolutionJs = _untrustedLvl11SolutionJs;
@synthesize aZzJPLMarkdown = _aZzJPLMarkdown;
@synthesize javaMergeTwoArray = _javaMergeTwoArray;
@synthesize wurstClientV2212ErrorReportMd = _wurstClientV2212ErrorReportMd;
@synthesize gistfile1Txt = _gistfile1Txt;
@synthesize codingReferenceSheet = _codingReferenceSheet;
@synthesize styles = _styles;
@synthesize x1RegistrySnapshotXml = x1RegistrySnapshotXml;
@synthesize summaryMd = _summaryMd;
@synthesize untrustedLvl1SolutionJs = _untrustedLvl1SolutionJs;
@synthesize tributePageMarkdown = _tributePageMarkdown;
@synthesize installTxt = _installTxt;
@synthesize untrustedLvl14SolutionJs = _untrustedLvl14SolutionJs;
@synthesize reflectPy = _reflectPy;
@synthesize dataJs = _dataJs;
@synthesize codeJs = _codeJs;
@synthesize filesSnapshotXml = _filesSnapshotXml;
@synthesize microProcessEngine = _microProcessEngine;
@synthesize indexHtml = _indexHtml;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.kwargsOnDecoratedFuncsIpynb = [ACCKwargsOnDecoratedFuncsIpynb modelObjectWithDictionary:[dict objectForKey:kACCFilesKwargsOnDecoratedFuncsIpynb]];
            self.openpanzerSaveJson = [ACCOpenpanzerSaveJson modelObjectWithDictionary:[dict objectForKey:kACCFilesOpenpanzerSaveJson]];
            self.configJson = [ACCConfigJson modelObjectWithDictionary:[dict objectForKey:kACCFilesConfigJson]];
            self.rELKSh = [ACCRELKSh modelObjectWithDictionary:[dict objectForKey:kACCFilesRELKSh]];
            self.uninstallTxt = [ACCUninstallTxt modelObjectWithDictionary:[dict objectForKey:kACCFilesUninstallTxt]];
            self.inspectUriClassPropertiesCs = [ACCInspectUriClassPropertiesCs modelObjectWithDictionary:[dict objectForKey:kACCFilesInspectUriClassPropertiesCs]];
            self.myCss = [ACCMyCss modelObjectWithDictionary:[dict objectForKey:kACCFilesMyCss]];
            self.untrustedLvl11SolutionJs = [ACCUntrustedLvl11SolutionJs modelObjectWithDictionary:[dict objectForKey:kACCFilesUntrustedLvl11SolutionJs]];
            self.aZzJPLMarkdown = [ACCAZzJPLMarkdown modelObjectWithDictionary:[dict objectForKey:kACCFilesAZzJPLMarkdown]];
            self.javaMergeTwoArray = [ACCJavaMergeTwoArray modelObjectWithDictionary:[dict objectForKey:kACCFilesJavaMergeTwoArray]];
            self.wurstClientV2212ErrorReportMd = [ACCWurstClientV2212ErrorReportMd modelObjectWithDictionary:[dict objectForKey:kACCFilesWurstClientV2212ErrorReportMd]];
            self.gistfile1Txt = [ACCGistfile1Txt modelObjectWithDictionary:[dict objectForKey:kACCFilesGistfile1Txt]];
            self.codingReferenceSheet = [ACCCodingReferenceSheet modelObjectWithDictionary:[dict objectForKey:kACCFilesCodingReferenceSheet]];
            self.styles = [ACCStyles modelObjectWithDictionary:[dict objectForKey:kACCFilesStyles]];
            self.x1RegistrySnapshotXml = [ACC1RegistrySnapshotXml modelObjectWithDictionary:[dict objectForKey:kACCFiles1RegistrySnapshotXml]];
            self.summaryMd = [ACCSummaryMd modelObjectWithDictionary:[dict objectForKey:kACCFilesSummaryMd]];
            self.untrustedLvl1SolutionJs = [ACCUntrustedLvl1SolutionJs modelObjectWithDictionary:[dict objectForKey:kACCFilesUntrustedLvl1SolutionJs]];
            self.tributePageMarkdown = [ACCTributePageMarkdown modelObjectWithDictionary:[dict objectForKey:kACCFilesTributePageMarkdown]];
            self.installTxt = [ACCInstallTxt modelObjectWithDictionary:[dict objectForKey:kACCFilesInstallTxt]];
            self.untrustedLvl14SolutionJs = [ACCUntrustedLvl14SolutionJs modelObjectWithDictionary:[dict objectForKey:kACCFilesUntrustedLvl14SolutionJs]];
            self.reflectPy = [ACCReflectPy modelObjectWithDictionary:[dict objectForKey:kACCFilesReflectPy]];
            self.dataJs = [ACCDataJs modelObjectWithDictionary:[dict objectForKey:kACCFilesDataJs]];
            self.codeJs = [ACCCodeJs modelObjectWithDictionary:[dict objectForKey:kACCFilesCodeJs]];
            self.filesSnapshotXml = [ACCFilesSnapshotXml modelObjectWithDictionary:[dict objectForKey:kACCFilesFilesSnapshotXml]];
            self.microProcessEngine = [ACCMicroProcessEngine modelObjectWithDictionary:[dict objectForKey:kACCFilesMicroProcessEngine]];
            self.indexHtml = [ACCIndexHtml modelObjectWithDictionary:[dict objectForKey:kACCFilesIndexHtml]];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[self.kwargsOnDecoratedFuncsIpynb dictionaryRepresentation] forKey:kACCFilesKwargsOnDecoratedFuncsIpynb];
    [mutableDict setValue:[self.openpanzerSaveJson dictionaryRepresentation] forKey:kACCFilesOpenpanzerSaveJson];
    [mutableDict setValue:[self.configJson dictionaryRepresentation] forKey:kACCFilesConfigJson];
    [mutableDict setValue:[self.rELKSh dictionaryRepresentation] forKey:kACCFilesRELKSh];
    [mutableDict setValue:[self.uninstallTxt dictionaryRepresentation] forKey:kACCFilesUninstallTxt];
    [mutableDict setValue:[self.inspectUriClassPropertiesCs dictionaryRepresentation] forKey:kACCFilesInspectUriClassPropertiesCs];
    [mutableDict setValue:[self.myCss dictionaryRepresentation] forKey:kACCFilesMyCss];
    [mutableDict setValue:[self.untrustedLvl11SolutionJs dictionaryRepresentation] forKey:kACCFilesUntrustedLvl11SolutionJs];
    [mutableDict setValue:[self.aZzJPLMarkdown dictionaryRepresentation] forKey:kACCFilesAZzJPLMarkdown];
    [mutableDict setValue:[self.javaMergeTwoArray dictionaryRepresentation] forKey:kACCFilesJavaMergeTwoArray];
    [mutableDict setValue:[self.wurstClientV2212ErrorReportMd dictionaryRepresentation] forKey:kACCFilesWurstClientV2212ErrorReportMd];
    [mutableDict setValue:[self.gistfile1Txt dictionaryRepresentation] forKey:kACCFilesGistfile1Txt];
    [mutableDict setValue:[self.codingReferenceSheet dictionaryRepresentation] forKey:kACCFilesCodingReferenceSheet];
    [mutableDict setValue:[self.styles dictionaryRepresentation] forKey:kACCFilesStyles];
    [mutableDict setValue:[self.x1RegistrySnapshotXml dictionaryRepresentation] forKey:kACCFiles1RegistrySnapshotXml];
    [mutableDict setValue:[self.summaryMd dictionaryRepresentation] forKey:kACCFilesSummaryMd];
    [mutableDict setValue:[self.untrustedLvl1SolutionJs dictionaryRepresentation] forKey:kACCFilesUntrustedLvl1SolutionJs];
    [mutableDict setValue:[self.tributePageMarkdown dictionaryRepresentation] forKey:kACCFilesTributePageMarkdown];
    [mutableDict setValue:[self.installTxt dictionaryRepresentation] forKey:kACCFilesInstallTxt];
    [mutableDict setValue:[self.untrustedLvl14SolutionJs dictionaryRepresentation] forKey:kACCFilesUntrustedLvl14SolutionJs];
    [mutableDict setValue:[self.reflectPy dictionaryRepresentation] forKey:kACCFilesReflectPy];
    [mutableDict setValue:[self.dataJs dictionaryRepresentation] forKey:kACCFilesDataJs];
    [mutableDict setValue:[self.codeJs dictionaryRepresentation] forKey:kACCFilesCodeJs];
    [mutableDict setValue:[self.filesSnapshotXml dictionaryRepresentation] forKey:kACCFilesFilesSnapshotXml];
    [mutableDict setValue:[self.microProcessEngine dictionaryRepresentation] forKey:kACCFilesMicroProcessEngine];
    [mutableDict setValue:[self.indexHtml dictionaryRepresentation] forKey:kACCFilesIndexHtml];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.kwargsOnDecoratedFuncsIpynb = [aDecoder decodeObjectForKey:kACCFilesKwargsOnDecoratedFuncsIpynb];
    self.openpanzerSaveJson = [aDecoder decodeObjectForKey:kACCFilesOpenpanzerSaveJson];
    self.configJson = [aDecoder decodeObjectForKey:kACCFilesConfigJson];
    self.rELKSh = [aDecoder decodeObjectForKey:kACCFilesRELKSh];
    self.uninstallTxt = [aDecoder decodeObjectForKey:kACCFilesUninstallTxt];
    self.inspectUriClassPropertiesCs = [aDecoder decodeObjectForKey:kACCFilesInspectUriClassPropertiesCs];
    self.myCss = [aDecoder decodeObjectForKey:kACCFilesMyCss];
    self.untrustedLvl11SolutionJs = [aDecoder decodeObjectForKey:kACCFilesUntrustedLvl11SolutionJs];
    self.aZzJPLMarkdown = [aDecoder decodeObjectForKey:kACCFilesAZzJPLMarkdown];
    self.javaMergeTwoArray = [aDecoder decodeObjectForKey:kACCFilesJavaMergeTwoArray];
    self.wurstClientV2212ErrorReportMd = [aDecoder decodeObjectForKey:kACCFilesWurstClientV2212ErrorReportMd];
    self.gistfile1Txt = [aDecoder decodeObjectForKey:kACCFilesGistfile1Txt];
    self.codingReferenceSheet = [aDecoder decodeObjectForKey:kACCFilesCodingReferenceSheet];
    self.styles = [aDecoder decodeObjectForKey:kACCFilesStyles];
    self.x1RegistrySnapshotXml = [aDecoder decodeObjectForKey:kACCFiles1RegistrySnapshotXml];
    self.summaryMd = [aDecoder decodeObjectForKey:kACCFilesSummaryMd];
    self.untrustedLvl1SolutionJs = [aDecoder decodeObjectForKey:kACCFilesUntrustedLvl1SolutionJs];
    self.tributePageMarkdown = [aDecoder decodeObjectForKey:kACCFilesTributePageMarkdown];
    self.installTxt = [aDecoder decodeObjectForKey:kACCFilesInstallTxt];
    self.untrustedLvl14SolutionJs = [aDecoder decodeObjectForKey:kACCFilesUntrustedLvl14SolutionJs];
    self.reflectPy = [aDecoder decodeObjectForKey:kACCFilesReflectPy];
    self.dataJs = [aDecoder decodeObjectForKey:kACCFilesDataJs];
    self.codeJs = [aDecoder decodeObjectForKey:kACCFilesCodeJs];
    self.filesSnapshotXml = [aDecoder decodeObjectForKey:kACCFilesFilesSnapshotXml];
    self.microProcessEngine = [aDecoder decodeObjectForKey:kACCFilesMicroProcessEngine];
    self.indexHtml = [aDecoder decodeObjectForKey:kACCFilesIndexHtml];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_kwargsOnDecoratedFuncsIpynb forKey:kACCFilesKwargsOnDecoratedFuncsIpynb];
    [aCoder encodeObject:_openpanzerSaveJson forKey:kACCFilesOpenpanzerSaveJson];
    [aCoder encodeObject:_configJson forKey:kACCFilesConfigJson];
    [aCoder encodeObject:_rELKSh forKey:kACCFilesRELKSh];
    [aCoder encodeObject:_uninstallTxt forKey:kACCFilesUninstallTxt];
    [aCoder encodeObject:_inspectUriClassPropertiesCs forKey:kACCFilesInspectUriClassPropertiesCs];
    [aCoder encodeObject:_myCss forKey:kACCFilesMyCss];
    [aCoder encodeObject:_untrustedLvl11SolutionJs forKey:kACCFilesUntrustedLvl11SolutionJs];
    [aCoder encodeObject:_aZzJPLMarkdown forKey:kACCFilesAZzJPLMarkdown];
    [aCoder encodeObject:_javaMergeTwoArray forKey:kACCFilesJavaMergeTwoArray];
    [aCoder encodeObject:_wurstClientV2212ErrorReportMd forKey:kACCFilesWurstClientV2212ErrorReportMd];
    [aCoder encodeObject:_gistfile1Txt forKey:kACCFilesGistfile1Txt];
    [aCoder encodeObject:_codingReferenceSheet forKey:kACCFilesCodingReferenceSheet];
    [aCoder encodeObject:_styles forKey:kACCFilesStyles];
    [aCoder encodeObject:x1RegistrySnapshotXml forKey:kACCFiles1RegistrySnapshotXml];
    [aCoder encodeObject:_summaryMd forKey:kACCFilesSummaryMd];
    [aCoder encodeObject:_untrustedLvl1SolutionJs forKey:kACCFilesUntrustedLvl1SolutionJs];
    [aCoder encodeObject:_tributePageMarkdown forKey:kACCFilesTributePageMarkdown];
    [aCoder encodeObject:_installTxt forKey:kACCFilesInstallTxt];
    [aCoder encodeObject:_untrustedLvl14SolutionJs forKey:kACCFilesUntrustedLvl14SolutionJs];
    [aCoder encodeObject:_reflectPy forKey:kACCFilesReflectPy];
    [aCoder encodeObject:_dataJs forKey:kACCFilesDataJs];
    [aCoder encodeObject:_codeJs forKey:kACCFilesCodeJs];
    [aCoder encodeObject:_filesSnapshotXml forKey:kACCFilesFilesSnapshotXml];
    [aCoder encodeObject:_microProcessEngine forKey:kACCFilesMicroProcessEngine];
    [aCoder encodeObject:_indexHtml forKey:kACCFilesIndexHtml];
}

- (id)copyWithZone:(NSZone *)zone
{
    ACCFiles *copy = [[ACCFiles alloc] init];
    
    if (copy) {

        copy.kwargsOnDecoratedFuncsIpynb = [self.kwargsOnDecoratedFuncsIpynb copyWithZone:zone];
        copy.openpanzerSaveJson = [self.openpanzerSaveJson copyWithZone:zone];
        copy.configJson = [self.configJson copyWithZone:zone];
        copy.rELKSh = [self.rELKSh copyWithZone:zone];
        copy.uninstallTxt = [self.uninstallTxt copyWithZone:zone];
        copy.inspectUriClassPropertiesCs = [self.inspectUriClassPropertiesCs copyWithZone:zone];
        copy.myCss = [self.myCss copyWithZone:zone];
        copy.untrustedLvl11SolutionJs = [self.untrustedLvl11SolutionJs copyWithZone:zone];
        copy.aZzJPLMarkdown = [self.aZzJPLMarkdown copyWithZone:zone];
        copy.javaMergeTwoArray = [self.javaMergeTwoArray copyWithZone:zone];
        copy.wurstClientV2212ErrorReportMd = [self.wurstClientV2212ErrorReportMd copyWithZone:zone];
        copy.gistfile1Txt = [self.gistfile1Txt copyWithZone:zone];
        copy.codingReferenceSheet = [self.codingReferenceSheet copyWithZone:zone];
        copy.styles = [self.styles copyWithZone:zone];
        copy.x1RegistrySnapshotXml = [self.x1RegistrySnapshotXml copyWithZone:zone];
        copy.summaryMd = [self.summaryMd copyWithZone:zone];
        copy.untrustedLvl1SolutionJs = [self.untrustedLvl1SolutionJs copyWithZone:zone];
        copy.tributePageMarkdown = [self.tributePageMarkdown copyWithZone:zone];
        copy.installTxt = [self.installTxt copyWithZone:zone];
        copy.untrustedLvl14SolutionJs = [self.untrustedLvl14SolutionJs copyWithZone:zone];
        copy.reflectPy = [self.reflectPy copyWithZone:zone];
        copy.dataJs = [self.dataJs copyWithZone:zone];
        copy.codeJs = [self.codeJs copyWithZone:zone];
        copy.filesSnapshotXml = [self.filesSnapshotXml copyWithZone:zone];
        copy.microProcessEngine = [self.microProcessEngine copyWithZone:zone];
        copy.indexHtml = [self.indexHtml copyWithZone:zone];
    }
    
    return copy;
}


@end
