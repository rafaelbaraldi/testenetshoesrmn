//
//  ACCDataJs.m
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ACCDataJs.h"


NSString *const kACCDataJsRawUrl = @"raw_url";
NSString *const kACCDataJsLanguage = @"language";
NSString *const kACCDataJsFilename = @"filename";
NSString *const kACCDataJsSize = @"size";
NSString *const kACCDataJsType = @"type";


@interface ACCDataJs ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ACCDataJs

@synthesize rawUrl = _rawUrl;
@synthesize language = _language;
@synthesize filename = _filename;
@synthesize size = _size;
@synthesize type = _type;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.rawUrl = [self objectOrNilForKey:kACCDataJsRawUrl fromDictionary:dict];
            self.language = [self objectOrNilForKey:kACCDataJsLanguage fromDictionary:dict];
            self.filename = [self objectOrNilForKey:kACCDataJsFilename fromDictionary:dict];
            self.size = [[self objectOrNilForKey:kACCDataJsSize fromDictionary:dict] doubleValue];
            self.type = [self objectOrNilForKey:kACCDataJsType fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.rawUrl forKey:kACCDataJsRawUrl];
    [mutableDict setValue:self.language forKey:kACCDataJsLanguage];
    [mutableDict setValue:self.filename forKey:kACCDataJsFilename];
    [mutableDict setValue:[NSNumber numberWithDouble:self.size] forKey:kACCDataJsSize];
    [mutableDict setValue:self.type forKey:kACCDataJsType];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.rawUrl = [aDecoder decodeObjectForKey:kACCDataJsRawUrl];
    self.language = [aDecoder decodeObjectForKey:kACCDataJsLanguage];
    self.filename = [aDecoder decodeObjectForKey:kACCDataJsFilename];
    self.size = [aDecoder decodeDoubleForKey:kACCDataJsSize];
    self.type = [aDecoder decodeObjectForKey:kACCDataJsType];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_rawUrl forKey:kACCDataJsRawUrl];
    [aCoder encodeObject:_language forKey:kACCDataJsLanguage];
    [aCoder encodeObject:_filename forKey:kACCDataJsFilename];
    [aCoder encodeDouble:_size forKey:kACCDataJsSize];
    [aCoder encodeObject:_type forKey:kACCDataJsType];
}

- (id)copyWithZone:(NSZone *)zone
{
    ACCDataJs *copy = [[ACCDataJs alloc] init];
    
    if (copy) {

        copy.rawUrl = [self.rawUrl copyWithZone:zone];
        copy.language = [self.language copyWithZone:zone];
        copy.filename = [self.filename copyWithZone:zone];
        copy.size = self.size;
        copy.type = [self.type copyWithZone:zone];
    }
    
    return copy;
}


@end
