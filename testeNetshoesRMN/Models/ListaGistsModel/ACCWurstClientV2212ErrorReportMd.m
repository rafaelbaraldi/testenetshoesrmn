//
//  ACCWurstClientV2212ErrorReportMd.m
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ACCWurstClientV2212ErrorReportMd.h"


NSString *const kACCWurstClientV2212ErrorReportMdRawUrl = @"raw_url";
NSString *const kACCWurstClientV2212ErrorReportMdLanguage = @"language";
NSString *const kACCWurstClientV2212ErrorReportMdFilename = @"filename";
NSString *const kACCWurstClientV2212ErrorReportMdSize = @"size";
NSString *const kACCWurstClientV2212ErrorReportMdType = @"type";


@interface ACCWurstClientV2212ErrorReportMd ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ACCWurstClientV2212ErrorReportMd

@synthesize rawUrl = _rawUrl;
@synthesize language = _language;
@synthesize filename = _filename;
@synthesize size = _size;
@synthesize type = _type;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.rawUrl = [self objectOrNilForKey:kACCWurstClientV2212ErrorReportMdRawUrl fromDictionary:dict];
            self.language = [self objectOrNilForKey:kACCWurstClientV2212ErrorReportMdLanguage fromDictionary:dict];
            self.filename = [self objectOrNilForKey:kACCWurstClientV2212ErrorReportMdFilename fromDictionary:dict];
            self.size = [[self objectOrNilForKey:kACCWurstClientV2212ErrorReportMdSize fromDictionary:dict] doubleValue];
            self.type = [self objectOrNilForKey:kACCWurstClientV2212ErrorReportMdType fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.rawUrl forKey:kACCWurstClientV2212ErrorReportMdRawUrl];
    [mutableDict setValue:self.language forKey:kACCWurstClientV2212ErrorReportMdLanguage];
    [mutableDict setValue:self.filename forKey:kACCWurstClientV2212ErrorReportMdFilename];
    [mutableDict setValue:[NSNumber numberWithDouble:self.size] forKey:kACCWurstClientV2212ErrorReportMdSize];
    [mutableDict setValue:self.type forKey:kACCWurstClientV2212ErrorReportMdType];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.rawUrl = [aDecoder decodeObjectForKey:kACCWurstClientV2212ErrorReportMdRawUrl];
    self.language = [aDecoder decodeObjectForKey:kACCWurstClientV2212ErrorReportMdLanguage];
    self.filename = [aDecoder decodeObjectForKey:kACCWurstClientV2212ErrorReportMdFilename];
    self.size = [aDecoder decodeDoubleForKey:kACCWurstClientV2212ErrorReportMdSize];
    self.type = [aDecoder decodeObjectForKey:kACCWurstClientV2212ErrorReportMdType];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_rawUrl forKey:kACCWurstClientV2212ErrorReportMdRawUrl];
    [aCoder encodeObject:_language forKey:kACCWurstClientV2212ErrorReportMdLanguage];
    [aCoder encodeObject:_filename forKey:kACCWurstClientV2212ErrorReportMdFilename];
    [aCoder encodeDouble:_size forKey:kACCWurstClientV2212ErrorReportMdSize];
    [aCoder encodeObject:_type forKey:kACCWurstClientV2212ErrorReportMdType];
}

- (id)copyWithZone:(NSZone *)zone
{
    ACCWurstClientV2212ErrorReportMd *copy = [[ACCWurstClientV2212ErrorReportMd alloc] init];
    
    if (copy) {

        copy.rawUrl = [self.rawUrl copyWithZone:zone];
        copy.language = [self.language copyWithZone:zone];
        copy.filename = [self.filename copyWithZone:zone];
        copy.size = self.size;
        copy.type = [self.type copyWithZone:zone];
    }
    
    return copy;
}


@end
