//
//  ACCUntrustedLvl11SolutionJs.m
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ACCUntrustedLvl11SolutionJs.h"


NSString *const kACCUntrustedLvl11SolutionJsRawUrl = @"raw_url";
NSString *const kACCUntrustedLvl11SolutionJsLanguage = @"language";
NSString *const kACCUntrustedLvl11SolutionJsFilename = @"filename";
NSString *const kACCUntrustedLvl11SolutionJsSize = @"size";
NSString *const kACCUntrustedLvl11SolutionJsType = @"type";


@interface ACCUntrustedLvl11SolutionJs ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ACCUntrustedLvl11SolutionJs

@synthesize rawUrl = _rawUrl;
@synthesize language = _language;
@synthesize filename = _filename;
@synthesize size = _size;
@synthesize type = _type;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.rawUrl = [self objectOrNilForKey:kACCUntrustedLvl11SolutionJsRawUrl fromDictionary:dict];
            self.language = [self objectOrNilForKey:kACCUntrustedLvl11SolutionJsLanguage fromDictionary:dict];
            self.filename = [self objectOrNilForKey:kACCUntrustedLvl11SolutionJsFilename fromDictionary:dict];
            self.size = [[self objectOrNilForKey:kACCUntrustedLvl11SolutionJsSize fromDictionary:dict] doubleValue];
            self.type = [self objectOrNilForKey:kACCUntrustedLvl11SolutionJsType fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.rawUrl forKey:kACCUntrustedLvl11SolutionJsRawUrl];
    [mutableDict setValue:self.language forKey:kACCUntrustedLvl11SolutionJsLanguage];
    [mutableDict setValue:self.filename forKey:kACCUntrustedLvl11SolutionJsFilename];
    [mutableDict setValue:[NSNumber numberWithDouble:self.size] forKey:kACCUntrustedLvl11SolutionJsSize];
    [mutableDict setValue:self.type forKey:kACCUntrustedLvl11SolutionJsType];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.rawUrl = [aDecoder decodeObjectForKey:kACCUntrustedLvl11SolutionJsRawUrl];
    self.language = [aDecoder decodeObjectForKey:kACCUntrustedLvl11SolutionJsLanguage];
    self.filename = [aDecoder decodeObjectForKey:kACCUntrustedLvl11SolutionJsFilename];
    self.size = [aDecoder decodeDoubleForKey:kACCUntrustedLvl11SolutionJsSize];
    self.type = [aDecoder decodeObjectForKey:kACCUntrustedLvl11SolutionJsType];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_rawUrl forKey:kACCUntrustedLvl11SolutionJsRawUrl];
    [aCoder encodeObject:_language forKey:kACCUntrustedLvl11SolutionJsLanguage];
    [aCoder encodeObject:_filename forKey:kACCUntrustedLvl11SolutionJsFilename];
    [aCoder encodeDouble:_size forKey:kACCUntrustedLvl11SolutionJsSize];
    [aCoder encodeObject:_type forKey:kACCUntrustedLvl11SolutionJsType];
}

- (id)copyWithZone:(NSZone *)zone
{
    ACCUntrustedLvl11SolutionJs *copy = [[ACCUntrustedLvl11SolutionJs alloc] init];
    
    if (copy) {

        copy.rawUrl = [self.rawUrl copyWithZone:zone];
        copy.language = [self.language copyWithZone:zone];
        copy.filename = [self.filename copyWithZone:zone];
        copy.size = self.size;
        copy.type = [self.type copyWithZone:zone];
    }
    
    return copy;
}


@end
