//
//  ACCInspectUriClassPropertiesCs.m
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ACCInspectUriClassPropertiesCs.h"


NSString *const kACCInspectUriClassPropertiesCsRawUrl = @"raw_url";
NSString *const kACCInspectUriClassPropertiesCsLanguage = @"language";
NSString *const kACCInspectUriClassPropertiesCsFilename = @"filename";
NSString *const kACCInspectUriClassPropertiesCsSize = @"size";
NSString *const kACCInspectUriClassPropertiesCsType = @"type";


@interface ACCInspectUriClassPropertiesCs ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ACCInspectUriClassPropertiesCs

@synthesize rawUrl = _rawUrl;
@synthesize language = _language;
@synthesize filename = _filename;
@synthesize size = _size;
@synthesize type = _type;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.rawUrl = [self objectOrNilForKey:kACCInspectUriClassPropertiesCsRawUrl fromDictionary:dict];
            self.language = [self objectOrNilForKey:kACCInspectUriClassPropertiesCsLanguage fromDictionary:dict];
            self.filename = [self objectOrNilForKey:kACCInspectUriClassPropertiesCsFilename fromDictionary:dict];
            self.size = [[self objectOrNilForKey:kACCInspectUriClassPropertiesCsSize fromDictionary:dict] doubleValue];
            self.type = [self objectOrNilForKey:kACCInspectUriClassPropertiesCsType fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.rawUrl forKey:kACCInspectUriClassPropertiesCsRawUrl];
    [mutableDict setValue:self.language forKey:kACCInspectUriClassPropertiesCsLanguage];
    [mutableDict setValue:self.filename forKey:kACCInspectUriClassPropertiesCsFilename];
    [mutableDict setValue:[NSNumber numberWithDouble:self.size] forKey:kACCInspectUriClassPropertiesCsSize];
    [mutableDict setValue:self.type forKey:kACCInspectUriClassPropertiesCsType];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.rawUrl = [aDecoder decodeObjectForKey:kACCInspectUriClassPropertiesCsRawUrl];
    self.language = [aDecoder decodeObjectForKey:kACCInspectUriClassPropertiesCsLanguage];
    self.filename = [aDecoder decodeObjectForKey:kACCInspectUriClassPropertiesCsFilename];
    self.size = [aDecoder decodeDoubleForKey:kACCInspectUriClassPropertiesCsSize];
    self.type = [aDecoder decodeObjectForKey:kACCInspectUriClassPropertiesCsType];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_rawUrl forKey:kACCInspectUriClassPropertiesCsRawUrl];
    [aCoder encodeObject:_language forKey:kACCInspectUriClassPropertiesCsLanguage];
    [aCoder encodeObject:_filename forKey:kACCInspectUriClassPropertiesCsFilename];
    [aCoder encodeDouble:_size forKey:kACCInspectUriClassPropertiesCsSize];
    [aCoder encodeObject:_type forKey:kACCInspectUriClassPropertiesCsType];
}

- (id)copyWithZone:(NSZone *)zone
{
    ACCInspectUriClassPropertiesCs *copy = [[ACCInspectUriClassPropertiesCs alloc] init];
    
    if (copy) {

        copy.rawUrl = [self.rawUrl copyWithZone:zone];
        copy.language = [self.language copyWithZone:zone];
        copy.filename = [self.filename copyWithZone:zone];
        copy.size = self.size;
        copy.type = [self.type copyWithZone:zone];
    }
    
    return copy;
}


@end
