//
//  ACCUntrustedLvl14SolutionJs.m
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ACCUntrustedLvl14SolutionJs.h"


NSString *const kACCUntrustedLvl14SolutionJsRawUrl = @"raw_url";
NSString *const kACCUntrustedLvl14SolutionJsLanguage = @"language";
NSString *const kACCUntrustedLvl14SolutionJsFilename = @"filename";
NSString *const kACCUntrustedLvl14SolutionJsSize = @"size";
NSString *const kACCUntrustedLvl14SolutionJsType = @"type";


@interface ACCUntrustedLvl14SolutionJs ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ACCUntrustedLvl14SolutionJs

@synthesize rawUrl = _rawUrl;
@synthesize language = _language;
@synthesize filename = _filename;
@synthesize size = _size;
@synthesize type = _type;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.rawUrl = [self objectOrNilForKey:kACCUntrustedLvl14SolutionJsRawUrl fromDictionary:dict];
            self.language = [self objectOrNilForKey:kACCUntrustedLvl14SolutionJsLanguage fromDictionary:dict];
            self.filename = [self objectOrNilForKey:kACCUntrustedLvl14SolutionJsFilename fromDictionary:dict];
            self.size = [[self objectOrNilForKey:kACCUntrustedLvl14SolutionJsSize fromDictionary:dict] doubleValue];
            self.type = [self objectOrNilForKey:kACCUntrustedLvl14SolutionJsType fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.rawUrl forKey:kACCUntrustedLvl14SolutionJsRawUrl];
    [mutableDict setValue:self.language forKey:kACCUntrustedLvl14SolutionJsLanguage];
    [mutableDict setValue:self.filename forKey:kACCUntrustedLvl14SolutionJsFilename];
    [mutableDict setValue:[NSNumber numberWithDouble:self.size] forKey:kACCUntrustedLvl14SolutionJsSize];
    [mutableDict setValue:self.type forKey:kACCUntrustedLvl14SolutionJsType];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.rawUrl = [aDecoder decodeObjectForKey:kACCUntrustedLvl14SolutionJsRawUrl];
    self.language = [aDecoder decodeObjectForKey:kACCUntrustedLvl14SolutionJsLanguage];
    self.filename = [aDecoder decodeObjectForKey:kACCUntrustedLvl14SolutionJsFilename];
    self.size = [aDecoder decodeDoubleForKey:kACCUntrustedLvl14SolutionJsSize];
    self.type = [aDecoder decodeObjectForKey:kACCUntrustedLvl14SolutionJsType];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_rawUrl forKey:kACCUntrustedLvl14SolutionJsRawUrl];
    [aCoder encodeObject:_language forKey:kACCUntrustedLvl14SolutionJsLanguage];
    [aCoder encodeObject:_filename forKey:kACCUntrustedLvl14SolutionJsFilename];
    [aCoder encodeDouble:_size forKey:kACCUntrustedLvl14SolutionJsSize];
    [aCoder encodeObject:_type forKey:kACCUntrustedLvl14SolutionJsType];
}

- (id)copyWithZone:(NSZone *)zone
{
    ACCUntrustedLvl14SolutionJs *copy = [[ACCUntrustedLvl14SolutionJs alloc] init];
    
    if (copy) {

        copy.rawUrl = [self.rawUrl copyWithZone:zone];
        copy.language = [self.language copyWithZone:zone];
        copy.filename = [self.filename copyWithZone:zone];
        copy.size = self.size;
        copy.type = [self.type copyWithZone:zone];
    }
    
    return copy;
}


@end
