//
//  ACCStyles.h
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface ACCStyles : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *rawUrl;
@property (nonatomic, assign) id language;
@property (nonatomic, strong) NSString *filename;
@property (nonatomic, assign) double size;
@property (nonatomic, strong) NSString *type;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
