//
//  ACCTributePageMarkdown.m
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ACCTributePageMarkdown.h"


NSString *const kACCTributePageMarkdownRawUrl = @"raw_url";
NSString *const kACCTributePageMarkdownLanguage = @"language";
NSString *const kACCTributePageMarkdownFilename = @"filename";
NSString *const kACCTributePageMarkdownSize = @"size";
NSString *const kACCTributePageMarkdownType = @"type";


@interface ACCTributePageMarkdown ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ACCTributePageMarkdown

@synthesize rawUrl = _rawUrl;
@synthesize language = _language;
@synthesize filename = _filename;
@synthesize size = _size;
@synthesize type = _type;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.rawUrl = [self objectOrNilForKey:kACCTributePageMarkdownRawUrl fromDictionary:dict];
            self.language = [self objectOrNilForKey:kACCTributePageMarkdownLanguage fromDictionary:dict];
            self.filename = [self objectOrNilForKey:kACCTributePageMarkdownFilename fromDictionary:dict];
            self.size = [[self objectOrNilForKey:kACCTributePageMarkdownSize fromDictionary:dict] doubleValue];
            self.type = [self objectOrNilForKey:kACCTributePageMarkdownType fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.rawUrl forKey:kACCTributePageMarkdownRawUrl];
    [mutableDict setValue:self.language forKey:kACCTributePageMarkdownLanguage];
    [mutableDict setValue:self.filename forKey:kACCTributePageMarkdownFilename];
    [mutableDict setValue:[NSNumber numberWithDouble:self.size] forKey:kACCTributePageMarkdownSize];
    [mutableDict setValue:self.type forKey:kACCTributePageMarkdownType];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.rawUrl = [aDecoder decodeObjectForKey:kACCTributePageMarkdownRawUrl];
    self.language = [aDecoder decodeObjectForKey:kACCTributePageMarkdownLanguage];
    self.filename = [aDecoder decodeObjectForKey:kACCTributePageMarkdownFilename];
    self.size = [aDecoder decodeDoubleForKey:kACCTributePageMarkdownSize];
    self.type = [aDecoder decodeObjectForKey:kACCTributePageMarkdownType];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_rawUrl forKey:kACCTributePageMarkdownRawUrl];
    [aCoder encodeObject:_language forKey:kACCTributePageMarkdownLanguage];
    [aCoder encodeObject:_filename forKey:kACCTributePageMarkdownFilename];
    [aCoder encodeDouble:_size forKey:kACCTributePageMarkdownSize];
    [aCoder encodeObject:_type forKey:kACCTributePageMarkdownType];
}

- (id)copyWithZone:(NSZone *)zone
{
    ACCTributePageMarkdown *copy = [[ACCTributePageMarkdown alloc] init];
    
    if (copy) {

        copy.rawUrl = [self.rawUrl copyWithZone:zone];
        copy.language = [self.language copyWithZone:zone];
        copy.filename = [self.filename copyWithZone:zone];
        copy.size = self.size;
        copy.type = [self.type copyWithZone:zone];
    }
    
    return copy;
}


@end
