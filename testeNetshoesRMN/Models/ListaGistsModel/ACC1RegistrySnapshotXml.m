//
//  ACC1RegistrySnapshotXml.m
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ACC1RegistrySnapshotXml.h"


NSString *const kACC1RegistrySnapshotXmlRawUrl = @"raw_url";
NSString *const kACC1RegistrySnapshotXmlLanguage = @"language";
NSString *const kACC1RegistrySnapshotXmlFilename = @"filename";
NSString *const kACC1RegistrySnapshotXmlSize = @"size";
NSString *const kACC1RegistrySnapshotXmlType = @"type";


@interface ACC1RegistrySnapshotXml ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ACC1RegistrySnapshotXml

@synthesize rawUrl = _rawUrl;
@synthesize language = _language;
@synthesize filename = _filename;
@synthesize size = _size;
@synthesize type = _type;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.rawUrl = [self objectOrNilForKey:kACC1RegistrySnapshotXmlRawUrl fromDictionary:dict];
            self.language = [self objectOrNilForKey:kACC1RegistrySnapshotXmlLanguage fromDictionary:dict];
            self.filename = [self objectOrNilForKey:kACC1RegistrySnapshotXmlFilename fromDictionary:dict];
            self.size = [[self objectOrNilForKey:kACC1RegistrySnapshotXmlSize fromDictionary:dict] doubleValue];
            self.type = [self objectOrNilForKey:kACC1RegistrySnapshotXmlType fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.rawUrl forKey:kACC1RegistrySnapshotXmlRawUrl];
    [mutableDict setValue:self.language forKey:kACC1RegistrySnapshotXmlLanguage];
    [mutableDict setValue:self.filename forKey:kACC1RegistrySnapshotXmlFilename];
    [mutableDict setValue:[NSNumber numberWithDouble:self.size] forKey:kACC1RegistrySnapshotXmlSize];
    [mutableDict setValue:self.type forKey:kACC1RegistrySnapshotXmlType];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.rawUrl = [aDecoder decodeObjectForKey:kACC1RegistrySnapshotXmlRawUrl];
    self.language = [aDecoder decodeObjectForKey:kACC1RegistrySnapshotXmlLanguage];
    self.filename = [aDecoder decodeObjectForKey:kACC1RegistrySnapshotXmlFilename];
    self.size = [aDecoder decodeDoubleForKey:kACC1RegistrySnapshotXmlSize];
    self.type = [aDecoder decodeObjectForKey:kACC1RegistrySnapshotXmlType];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_rawUrl forKey:kACC1RegistrySnapshotXmlRawUrl];
    [aCoder encodeObject:_language forKey:kACC1RegistrySnapshotXmlLanguage];
    [aCoder encodeObject:_filename forKey:kACC1RegistrySnapshotXmlFilename];
    [aCoder encodeDouble:_size forKey:kACC1RegistrySnapshotXmlSize];
    [aCoder encodeObject:_type forKey:kACC1RegistrySnapshotXmlType];
}

- (id)copyWithZone:(NSZone *)zone
{
    ACC1RegistrySnapshotXml *copy = [[ACC1RegistrySnapshotXml alloc] init];
    
    if (copy) {

        copy.rawUrl = [self.rawUrl copyWithZone:zone];
        copy.language = [self.language copyWithZone:zone];
        copy.filename = [self.filename copyWithZone:zone];
        copy.size = self.size;
        copy.type = [self.type copyWithZone:zone];
    }
    
    return copy;
}


@end
