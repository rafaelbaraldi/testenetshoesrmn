//
//  ACCFilesSnapshotXml.m
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ACCFilesSnapshotXml.h"


NSString *const kACCFilesSnapshotXmlRawUrl = @"raw_url";
NSString *const kACCFilesSnapshotXmlLanguage = @"language";
NSString *const kACCFilesSnapshotXmlFilename = @"filename";
NSString *const kACCFilesSnapshotXmlSize = @"size";
NSString *const kACCFilesSnapshotXmlType = @"type";


@interface ACCFilesSnapshotXml ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ACCFilesSnapshotXml

@synthesize rawUrl = _rawUrl;
@synthesize language = _language;
@synthesize filename = _filename;
@synthesize size = _size;
@synthesize type = _type;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.rawUrl = [self objectOrNilForKey:kACCFilesSnapshotXmlRawUrl fromDictionary:dict];
            self.language = [self objectOrNilForKey:kACCFilesSnapshotXmlLanguage fromDictionary:dict];
            self.filename = [self objectOrNilForKey:kACCFilesSnapshotXmlFilename fromDictionary:dict];
            self.size = [[self objectOrNilForKey:kACCFilesSnapshotXmlSize fromDictionary:dict] doubleValue];
            self.type = [self objectOrNilForKey:kACCFilesSnapshotXmlType fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.rawUrl forKey:kACCFilesSnapshotXmlRawUrl];
    [mutableDict setValue:self.language forKey:kACCFilesSnapshotXmlLanguage];
    [mutableDict setValue:self.filename forKey:kACCFilesSnapshotXmlFilename];
    [mutableDict setValue:[NSNumber numberWithDouble:self.size] forKey:kACCFilesSnapshotXmlSize];
    [mutableDict setValue:self.type forKey:kACCFilesSnapshotXmlType];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.rawUrl = [aDecoder decodeObjectForKey:kACCFilesSnapshotXmlRawUrl];
    self.language = [aDecoder decodeObjectForKey:kACCFilesSnapshotXmlLanguage];
    self.filename = [aDecoder decodeObjectForKey:kACCFilesSnapshotXmlFilename];
    self.size = [aDecoder decodeDoubleForKey:kACCFilesSnapshotXmlSize];
    self.type = [aDecoder decodeObjectForKey:kACCFilesSnapshotXmlType];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_rawUrl forKey:kACCFilesSnapshotXmlRawUrl];
    [aCoder encodeObject:_language forKey:kACCFilesSnapshotXmlLanguage];
    [aCoder encodeObject:_filename forKey:kACCFilesSnapshotXmlFilename];
    [aCoder encodeDouble:_size forKey:kACCFilesSnapshotXmlSize];
    [aCoder encodeObject:_type forKey:kACCFilesSnapshotXmlType];
}

- (id)copyWithZone:(NSZone *)zone
{
    ACCFilesSnapshotXml *copy = [[ACCFilesSnapshotXml alloc] init];
    
    if (copy) {

        copy.rawUrl = [self.rawUrl copyWithZone:zone];
        copy.language = [self.language copyWithZone:zone];
        copy.filename = [self.filename copyWithZone:zone];
        copy.size = self.size;
        copy.type = [self.type copyWithZone:zone];
    }
    
    return copy;
}


@end
