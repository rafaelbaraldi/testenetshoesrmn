//
//  ACCUninstallTxt.m
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ACCUninstallTxt.h"


NSString *const kACCUninstallTxtRawUrl = @"raw_url";
NSString *const kACCUninstallTxtLanguage = @"language";
NSString *const kACCUninstallTxtFilename = @"filename";
NSString *const kACCUninstallTxtSize = @"size";
NSString *const kACCUninstallTxtType = @"type";


@interface ACCUninstallTxt ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ACCUninstallTxt

@synthesize rawUrl = _rawUrl;
@synthesize language = _language;
@synthesize filename = _filename;
@synthesize size = _size;
@synthesize type = _type;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.rawUrl = [self objectOrNilForKey:kACCUninstallTxtRawUrl fromDictionary:dict];
            self.language = [self objectOrNilForKey:kACCUninstallTxtLanguage fromDictionary:dict];
            self.filename = [self objectOrNilForKey:kACCUninstallTxtFilename fromDictionary:dict];
            self.size = [[self objectOrNilForKey:kACCUninstallTxtSize fromDictionary:dict] doubleValue];
            self.type = [self objectOrNilForKey:kACCUninstallTxtType fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.rawUrl forKey:kACCUninstallTxtRawUrl];
    [mutableDict setValue:self.language forKey:kACCUninstallTxtLanguage];
    [mutableDict setValue:self.filename forKey:kACCUninstallTxtFilename];
    [mutableDict setValue:[NSNumber numberWithDouble:self.size] forKey:kACCUninstallTxtSize];
    [mutableDict setValue:self.type forKey:kACCUninstallTxtType];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.rawUrl = [aDecoder decodeObjectForKey:kACCUninstallTxtRawUrl];
    self.language = [aDecoder decodeObjectForKey:kACCUninstallTxtLanguage];
    self.filename = [aDecoder decodeObjectForKey:kACCUninstallTxtFilename];
    self.size = [aDecoder decodeDoubleForKey:kACCUninstallTxtSize];
    self.type = [aDecoder decodeObjectForKey:kACCUninstallTxtType];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_rawUrl forKey:kACCUninstallTxtRawUrl];
    [aCoder encodeObject:_language forKey:kACCUninstallTxtLanguage];
    [aCoder encodeObject:_filename forKey:kACCUninstallTxtFilename];
    [aCoder encodeDouble:_size forKey:kACCUninstallTxtSize];
    [aCoder encodeObject:_type forKey:kACCUninstallTxtType];
}

- (id)copyWithZone:(NSZone *)zone
{
    ACCUninstallTxt *copy = [[ACCUninstallTxt alloc] init];
    
    if (copy) {

        copy.rawUrl = [self.rawUrl copyWithZone:zone];
        copy.language = [self.language copyWithZone:zone];
        copy.filename = [self.filename copyWithZone:zone];
        copy.size = self.size;
        copy.type = [self.type copyWithZone:zone];
    }
    
    return copy;
}


@end
