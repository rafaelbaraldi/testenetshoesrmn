//
//  ACCJavaMergeTwoArray.m
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ACCJavaMergeTwoArray.h"


NSString *const kACCJavaMergeTwoArrayRawUrl = @"raw_url";
NSString *const kACCJavaMergeTwoArrayLanguage = @"language";
NSString *const kACCJavaMergeTwoArrayFilename = @"filename";
NSString *const kACCJavaMergeTwoArraySize = @"size";
NSString *const kACCJavaMergeTwoArrayType = @"type";


@interface ACCJavaMergeTwoArray ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ACCJavaMergeTwoArray

@synthesize rawUrl = _rawUrl;
@synthesize language = _language;
@synthesize filename = _filename;
@synthesize size = _size;
@synthesize type = _type;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.rawUrl = [self objectOrNilForKey:kACCJavaMergeTwoArrayRawUrl fromDictionary:dict];
            self.language = [self objectOrNilForKey:kACCJavaMergeTwoArrayLanguage fromDictionary:dict];
            self.filename = [self objectOrNilForKey:kACCJavaMergeTwoArrayFilename fromDictionary:dict];
            self.size = [[self objectOrNilForKey:kACCJavaMergeTwoArraySize fromDictionary:dict] doubleValue];
            self.type = [self objectOrNilForKey:kACCJavaMergeTwoArrayType fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.rawUrl forKey:kACCJavaMergeTwoArrayRawUrl];
    [mutableDict setValue:self.language forKey:kACCJavaMergeTwoArrayLanguage];
    [mutableDict setValue:self.filename forKey:kACCJavaMergeTwoArrayFilename];
    [mutableDict setValue:[NSNumber numberWithDouble:self.size] forKey:kACCJavaMergeTwoArraySize];
    [mutableDict setValue:self.type forKey:kACCJavaMergeTwoArrayType];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.rawUrl = [aDecoder decodeObjectForKey:kACCJavaMergeTwoArrayRawUrl];
    self.language = [aDecoder decodeObjectForKey:kACCJavaMergeTwoArrayLanguage];
    self.filename = [aDecoder decodeObjectForKey:kACCJavaMergeTwoArrayFilename];
    self.size = [aDecoder decodeDoubleForKey:kACCJavaMergeTwoArraySize];
    self.type = [aDecoder decodeObjectForKey:kACCJavaMergeTwoArrayType];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_rawUrl forKey:kACCJavaMergeTwoArrayRawUrl];
    [aCoder encodeObject:_language forKey:kACCJavaMergeTwoArrayLanguage];
    [aCoder encodeObject:_filename forKey:kACCJavaMergeTwoArrayFilename];
    [aCoder encodeDouble:_size forKey:kACCJavaMergeTwoArraySize];
    [aCoder encodeObject:_type forKey:kACCJavaMergeTwoArrayType];
}

- (id)copyWithZone:(NSZone *)zone
{
    ACCJavaMergeTwoArray *copy = [[ACCJavaMergeTwoArray alloc] init];
    
    if (copy) {

        copy.rawUrl = [self.rawUrl copyWithZone:zone];
        copy.language = [self.language copyWithZone:zone];
        copy.filename = [self.filename copyWithZone:zone];
        copy.size = self.size;
        copy.type = [self.type copyWithZone:zone];
    }
    
    return copy;
}


@end
