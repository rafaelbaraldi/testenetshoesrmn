//
//  ACCRELKSh.m
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ACCRELKSh.h"


NSString *const kACCRELKShRawUrl = @"raw_url";
NSString *const kACCRELKShLanguage = @"language";
NSString *const kACCRELKShFilename = @"filename";
NSString *const kACCRELKShSize = @"size";
NSString *const kACCRELKShType = @"type";


@interface ACCRELKSh ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ACCRELKSh

@synthesize rawUrl = _rawUrl;
@synthesize language = _language;
@synthesize filename = _filename;
@synthesize size = _size;
@synthesize type = _type;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.rawUrl = [self objectOrNilForKey:kACCRELKShRawUrl fromDictionary:dict];
            self.language = [self objectOrNilForKey:kACCRELKShLanguage fromDictionary:dict];
            self.filename = [self objectOrNilForKey:kACCRELKShFilename fromDictionary:dict];
            self.size = [[self objectOrNilForKey:kACCRELKShSize fromDictionary:dict] doubleValue];
            self.type = [self objectOrNilForKey:kACCRELKShType fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.rawUrl forKey:kACCRELKShRawUrl];
    [mutableDict setValue:self.language forKey:kACCRELKShLanguage];
    [mutableDict setValue:self.filename forKey:kACCRELKShFilename];
    [mutableDict setValue:[NSNumber numberWithDouble:self.size] forKey:kACCRELKShSize];
    [mutableDict setValue:self.type forKey:kACCRELKShType];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.rawUrl = [aDecoder decodeObjectForKey:kACCRELKShRawUrl];
    self.language = [aDecoder decodeObjectForKey:kACCRELKShLanguage];
    self.filename = [aDecoder decodeObjectForKey:kACCRELKShFilename];
    self.size = [aDecoder decodeDoubleForKey:kACCRELKShSize];
    self.type = [aDecoder decodeObjectForKey:kACCRELKShType];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_rawUrl forKey:kACCRELKShRawUrl];
    [aCoder encodeObject:_language forKey:kACCRELKShLanguage];
    [aCoder encodeObject:_filename forKey:kACCRELKShFilename];
    [aCoder encodeDouble:_size forKey:kACCRELKShSize];
    [aCoder encodeObject:_type forKey:kACCRELKShType];
}

- (id)copyWithZone:(NSZone *)zone
{
    ACCRELKSh *copy = [[ACCRELKSh alloc] init];
    
    if (copy) {

        copy.rawUrl = [self.rawUrl copyWithZone:zone];
        copy.language = [self.language copyWithZone:zone];
        copy.filename = [self.filename copyWithZone:zone];
        copy.size = self.size;
        copy.type = [self.type copyWithZone:zone];
    }
    
    return copy;
}


@end
