//
//  ACCKwargsOnDecoratedFuncsIpynb.m
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ACCKwargsOnDecoratedFuncsIpynb.h"


NSString *const kACCKwargsOnDecoratedFuncsIpynbRawUrl = @"raw_url";
NSString *const kACCKwargsOnDecoratedFuncsIpynbLanguage = @"language";
NSString *const kACCKwargsOnDecoratedFuncsIpynbFilename = @"filename";
NSString *const kACCKwargsOnDecoratedFuncsIpynbSize = @"size";
NSString *const kACCKwargsOnDecoratedFuncsIpynbType = @"type";


@interface ACCKwargsOnDecoratedFuncsIpynb ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ACCKwargsOnDecoratedFuncsIpynb

@synthesize rawUrl = _rawUrl;
@synthesize language = _language;
@synthesize filename = _filename;
@synthesize size = _size;
@synthesize type = _type;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.rawUrl = [self objectOrNilForKey:kACCKwargsOnDecoratedFuncsIpynbRawUrl fromDictionary:dict];
            self.language = [self objectOrNilForKey:kACCKwargsOnDecoratedFuncsIpynbLanguage fromDictionary:dict];
            self.filename = [self objectOrNilForKey:kACCKwargsOnDecoratedFuncsIpynbFilename fromDictionary:dict];
            self.size = [[self objectOrNilForKey:kACCKwargsOnDecoratedFuncsIpynbSize fromDictionary:dict] doubleValue];
            self.type = [self objectOrNilForKey:kACCKwargsOnDecoratedFuncsIpynbType fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.rawUrl forKey:kACCKwargsOnDecoratedFuncsIpynbRawUrl];
    [mutableDict setValue:self.language forKey:kACCKwargsOnDecoratedFuncsIpynbLanguage];
    [mutableDict setValue:self.filename forKey:kACCKwargsOnDecoratedFuncsIpynbFilename];
    [mutableDict setValue:[NSNumber numberWithDouble:self.size] forKey:kACCKwargsOnDecoratedFuncsIpynbSize];
    [mutableDict setValue:self.type forKey:kACCKwargsOnDecoratedFuncsIpynbType];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.rawUrl = [aDecoder decodeObjectForKey:kACCKwargsOnDecoratedFuncsIpynbRawUrl];
    self.language = [aDecoder decodeObjectForKey:kACCKwargsOnDecoratedFuncsIpynbLanguage];
    self.filename = [aDecoder decodeObjectForKey:kACCKwargsOnDecoratedFuncsIpynbFilename];
    self.size = [aDecoder decodeDoubleForKey:kACCKwargsOnDecoratedFuncsIpynbSize];
    self.type = [aDecoder decodeObjectForKey:kACCKwargsOnDecoratedFuncsIpynbType];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_rawUrl forKey:kACCKwargsOnDecoratedFuncsIpynbRawUrl];
    [aCoder encodeObject:_language forKey:kACCKwargsOnDecoratedFuncsIpynbLanguage];
    [aCoder encodeObject:_filename forKey:kACCKwargsOnDecoratedFuncsIpynbFilename];
    [aCoder encodeDouble:_size forKey:kACCKwargsOnDecoratedFuncsIpynbSize];
    [aCoder encodeObject:_type forKey:kACCKwargsOnDecoratedFuncsIpynbType];
}

- (id)copyWithZone:(NSZone *)zone
{
    ACCKwargsOnDecoratedFuncsIpynb *copy = [[ACCKwargsOnDecoratedFuncsIpynb alloc] init];
    
    if (copy) {

        copy.rawUrl = [self.rawUrl copyWithZone:zone];
        copy.language = [self.language copyWithZone:zone];
        copy.filename = [self.filename copyWithZone:zone];
        copy.size = self.size;
        copy.type = [self.type copyWithZone:zone];
    }
    
    return copy;
}


@end
