//
//  ACCMyCss.m
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ACCMyCss.h"


NSString *const kACCMyCssRawUrl = @"raw_url";
NSString *const kACCMyCssLanguage = @"language";
NSString *const kACCMyCssFilename = @"filename";
NSString *const kACCMyCssSize = @"size";
NSString *const kACCMyCssType = @"type";


@interface ACCMyCss ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ACCMyCss

@synthesize rawUrl = _rawUrl;
@synthesize language = _language;
@synthesize filename = _filename;
@synthesize size = _size;
@synthesize type = _type;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.rawUrl = [self objectOrNilForKey:kACCMyCssRawUrl fromDictionary:dict];
            self.language = [self objectOrNilForKey:kACCMyCssLanguage fromDictionary:dict];
            self.filename = [self objectOrNilForKey:kACCMyCssFilename fromDictionary:dict];
            self.size = [[self objectOrNilForKey:kACCMyCssSize fromDictionary:dict] doubleValue];
            self.type = [self objectOrNilForKey:kACCMyCssType fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.rawUrl forKey:kACCMyCssRawUrl];
    [mutableDict setValue:self.language forKey:kACCMyCssLanguage];
    [mutableDict setValue:self.filename forKey:kACCMyCssFilename];
    [mutableDict setValue:[NSNumber numberWithDouble:self.size] forKey:kACCMyCssSize];
    [mutableDict setValue:self.type forKey:kACCMyCssType];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.rawUrl = [aDecoder decodeObjectForKey:kACCMyCssRawUrl];
    self.language = [aDecoder decodeObjectForKey:kACCMyCssLanguage];
    self.filename = [aDecoder decodeObjectForKey:kACCMyCssFilename];
    self.size = [aDecoder decodeDoubleForKey:kACCMyCssSize];
    self.type = [aDecoder decodeObjectForKey:kACCMyCssType];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_rawUrl forKey:kACCMyCssRawUrl];
    [aCoder encodeObject:_language forKey:kACCMyCssLanguage];
    [aCoder encodeObject:_filename forKey:kACCMyCssFilename];
    [aCoder encodeDouble:_size forKey:kACCMyCssSize];
    [aCoder encodeObject:_type forKey:kACCMyCssType];
}

- (id)copyWithZone:(NSZone *)zone
{
    ACCMyCss *copy = [[ACCMyCss alloc] init];
    
    if (copy) {

        copy.rawUrl = [self.rawUrl copyWithZone:zone];
        copy.language = [self.language copyWithZone:zone];
        copy.filename = [self.filename copyWithZone:zone];
        copy.size = self.size;
        copy.type = [self.type copyWithZone:zone];
    }
    
    return copy;
}


@end
