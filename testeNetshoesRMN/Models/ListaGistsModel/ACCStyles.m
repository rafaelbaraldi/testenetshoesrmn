//
//  ACCStyles.m
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ACCStyles.h"


NSString *const kACCStylesRawUrl = @"raw_url";
NSString *const kACCStylesLanguage = @"language";
NSString *const kACCStylesFilename = @"filename";
NSString *const kACCStylesSize = @"size";
NSString *const kACCStylesType = @"type";


@interface ACCStyles ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ACCStyles

@synthesize rawUrl = _rawUrl;
@synthesize language = _language;
@synthesize filename = _filename;
@synthesize size = _size;
@synthesize type = _type;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.rawUrl = [self objectOrNilForKey:kACCStylesRawUrl fromDictionary:dict];
            self.language = [self objectOrNilForKey:kACCStylesLanguage fromDictionary:dict];
            self.filename = [self objectOrNilForKey:kACCStylesFilename fromDictionary:dict];
            self.size = [[self objectOrNilForKey:kACCStylesSize fromDictionary:dict] doubleValue];
            self.type = [self objectOrNilForKey:kACCStylesType fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.rawUrl forKey:kACCStylesRawUrl];
    [mutableDict setValue:self.language forKey:kACCStylesLanguage];
    [mutableDict setValue:self.filename forKey:kACCStylesFilename];
    [mutableDict setValue:[NSNumber numberWithDouble:self.size] forKey:kACCStylesSize];
    [mutableDict setValue:self.type forKey:kACCStylesType];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.rawUrl = [aDecoder decodeObjectForKey:kACCStylesRawUrl];
    self.language = [aDecoder decodeObjectForKey:kACCStylesLanguage];
    self.filename = [aDecoder decodeObjectForKey:kACCStylesFilename];
    self.size = [aDecoder decodeDoubleForKey:kACCStylesSize];
    self.type = [aDecoder decodeObjectForKey:kACCStylesType];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_rawUrl forKey:kACCStylesRawUrl];
    [aCoder encodeObject:_language forKey:kACCStylesLanguage];
    [aCoder encodeObject:_filename forKey:kACCStylesFilename];
    [aCoder encodeDouble:_size forKey:kACCStylesSize];
    [aCoder encodeObject:_type forKey:kACCStylesType];
}

- (id)copyWithZone:(NSZone *)zone
{
    ACCStyles *copy = [[ACCStyles alloc] init];
    
    if (copy) {

        copy.rawUrl = [self.rawUrl copyWithZone:zone];
        copy.language = [self.language copyWithZone:zone];
        copy.filename = [self.filename copyWithZone:zone];
        copy.size = self.size;
        copy.type = [self.type copyWithZone:zone];
    }
    
    return copy;
}


@end
