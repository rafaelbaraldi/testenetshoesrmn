//
//  ACClistaGists.m
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ACClistaGists.h"
#import "ACCOwner.h"
#import "ACCFiles.h"


NSString *const kACClistaGistsId = @"id";
NSString *const kACClistaGistsComments = @"comments";
NSString *const kACClistaGistsDescription = @"description";
NSString *const kACClistaGistsCreatedAt = @"created_at";
NSString *const kACClistaGistsOwner = @"owner";
NSString *const kACClistaGistsUrl = @"url";
NSString *const kACClistaGistsGitPushUrl = @"git_push_url";
NSString *const kACClistaGistsFiles = @"files";
NSString *const kACClistaGistsHtmlUrl = @"html_url";
NSString *const kACClistaGistsGitPullUrl = @"git_pull_url";
NSString *const kACClistaGistsTruncated = @"truncated";
NSString *const kACClistaGistsUpdatedAt = @"updated_at";
NSString *const kACClistaGistsForksUrl = @"forks_url";
NSString *const kACClistaGistsCommitsUrl = @"commits_url";
NSString *const kACClistaGistsUser = @"user";
NSString *const kACClistaGistsPublic = @"public";
NSString *const kACClistaGistsCommentsUrl = @"comments_url";


@interface ACClistaGists ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ACClistaGists

@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize comments = _comments;
@synthesize internalBaseClassDescription = _internalBaseClassDescription;
@synthesize createdAt = _createdAt;
@synthesize owner = _owner;
@synthesize url = _url;
@synthesize gitPushUrl = _gitPushUrl;
@synthesize files = _files;
@synthesize htmlUrl = _htmlUrl;
@synthesize gitPullUrl = _gitPullUrl;
@synthesize truncated = _truncated;
@synthesize updatedAt = _updatedAt;
@synthesize forksUrl = _forksUrl;
@synthesize commitsUrl = _commitsUrl;
@synthesize user = _user;
@synthesize publicProperty = _publicProperty;
@synthesize commentsUrl = _commentsUrl;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.internalBaseClassIdentifier = [self objectOrNilForKey:kACClistaGistsId fromDictionary:dict];
            self.comments = [[self objectOrNilForKey:kACClistaGistsComments fromDictionary:dict] doubleValue];
            self.internalBaseClassDescription = [self objectOrNilForKey:kACClistaGistsDescription fromDictionary:dict];
            self.createdAt = [self objectOrNilForKey:kACClistaGistsCreatedAt fromDictionary:dict];
            self.owner = [ACCOwner modelObjectWithDictionary:[dict objectForKey:kACClistaGistsOwner]];
            self.url = [self objectOrNilForKey:kACClistaGistsUrl fromDictionary:dict];
            self.gitPushUrl = [self objectOrNilForKey:kACClistaGistsGitPushUrl fromDictionary:dict];
            self.files = [ACCFiles modelObjectWithDictionary:[dict objectForKey:kACClistaGistsFiles]];
            self.htmlUrl = [self objectOrNilForKey:kACClistaGistsHtmlUrl fromDictionary:dict];
            self.gitPullUrl = [self objectOrNilForKey:kACClistaGistsGitPullUrl fromDictionary:dict];
            self.truncated = [[self objectOrNilForKey:kACClistaGistsTruncated fromDictionary:dict] boolValue];
            self.updatedAt = [self objectOrNilForKey:kACClistaGistsUpdatedAt fromDictionary:dict];
            self.forksUrl = [self objectOrNilForKey:kACClistaGistsForksUrl fromDictionary:dict];
            self.commitsUrl = [self objectOrNilForKey:kACClistaGistsCommitsUrl fromDictionary:dict];
            self.user = [self objectOrNilForKey:kACClistaGistsUser fromDictionary:dict];
            self.publicProperty = [[self objectOrNilForKey:kACClistaGistsPublic fromDictionary:dict] boolValue];
            self.commentsUrl = [self objectOrNilForKey:kACClistaGistsCommentsUrl fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.internalBaseClassIdentifier forKey:kACClistaGistsId];
    [mutableDict setValue:[NSNumber numberWithDouble:self.comments] forKey:kACClistaGistsComments];
    [mutableDict setValue:self.internalBaseClassDescription forKey:kACClistaGistsDescription];
    [mutableDict setValue:self.createdAt forKey:kACClistaGistsCreatedAt];
    [mutableDict setValue:[self.owner dictionaryRepresentation] forKey:kACClistaGistsOwner];
    [mutableDict setValue:self.url forKey:kACClistaGistsUrl];
    [mutableDict setValue:self.gitPushUrl forKey:kACClistaGistsGitPushUrl];
    [mutableDict setValue:[self.files dictionaryRepresentation] forKey:kACClistaGistsFiles];
    [mutableDict setValue:self.htmlUrl forKey:kACClistaGistsHtmlUrl];
    [mutableDict setValue:self.gitPullUrl forKey:kACClistaGistsGitPullUrl];
    [mutableDict setValue:[NSNumber numberWithBool:self.truncated] forKey:kACClistaGistsTruncated];
    [mutableDict setValue:self.updatedAt forKey:kACClistaGistsUpdatedAt];
    [mutableDict setValue:self.forksUrl forKey:kACClistaGistsForksUrl];
    [mutableDict setValue:self.commitsUrl forKey:kACClistaGistsCommitsUrl];
    [mutableDict setValue:self.user forKey:kACClistaGistsUser];
    [mutableDict setValue:[NSNumber numberWithBool:self.publicProperty] forKey:kACClistaGistsPublic];
    [mutableDict setValue:self.commentsUrl forKey:kACClistaGistsCommentsUrl];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.internalBaseClassIdentifier = [aDecoder decodeObjectForKey:kACClistaGistsId];
    self.comments = [aDecoder decodeDoubleForKey:kACClistaGistsComments];
    self.internalBaseClassDescription = [aDecoder decodeObjectForKey:kACClistaGistsDescription];
    self.createdAt = [aDecoder decodeObjectForKey:kACClistaGistsCreatedAt];
    self.owner = [aDecoder decodeObjectForKey:kACClistaGistsOwner];
    self.url = [aDecoder decodeObjectForKey:kACClistaGistsUrl];
    self.gitPushUrl = [aDecoder decodeObjectForKey:kACClistaGistsGitPushUrl];
    self.files = [aDecoder decodeObjectForKey:kACClistaGistsFiles];
    self.htmlUrl = [aDecoder decodeObjectForKey:kACClistaGistsHtmlUrl];
    self.gitPullUrl = [aDecoder decodeObjectForKey:kACClistaGistsGitPullUrl];
    self.truncated = [aDecoder decodeBoolForKey:kACClistaGistsTruncated];
    self.updatedAt = [aDecoder decodeObjectForKey:kACClistaGistsUpdatedAt];
    self.forksUrl = [aDecoder decodeObjectForKey:kACClistaGistsForksUrl];
    self.commitsUrl = [aDecoder decodeObjectForKey:kACClistaGistsCommitsUrl];
    self.user = [aDecoder decodeObjectForKey:kACClistaGistsUser];
    self.publicProperty = [aDecoder decodeBoolForKey:kACClistaGistsPublic];
    self.commentsUrl = [aDecoder decodeObjectForKey:kACClistaGistsCommentsUrl];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_internalBaseClassIdentifier forKey:kACClistaGistsId];
    [aCoder encodeDouble:_comments forKey:kACClistaGistsComments];
    [aCoder encodeObject:_internalBaseClassDescription forKey:kACClistaGistsDescription];
    [aCoder encodeObject:_createdAt forKey:kACClistaGistsCreatedAt];
    [aCoder encodeObject:_owner forKey:kACClistaGistsOwner];
    [aCoder encodeObject:_url forKey:kACClistaGistsUrl];
    [aCoder encodeObject:_gitPushUrl forKey:kACClistaGistsGitPushUrl];
    [aCoder encodeObject:_files forKey:kACClistaGistsFiles];
    [aCoder encodeObject:_htmlUrl forKey:kACClistaGistsHtmlUrl];
    [aCoder encodeObject:_gitPullUrl forKey:kACClistaGistsGitPullUrl];
    [aCoder encodeBool:_truncated forKey:kACClistaGistsTruncated];
    [aCoder encodeObject:_updatedAt forKey:kACClistaGistsUpdatedAt];
    [aCoder encodeObject:_forksUrl forKey:kACClistaGistsForksUrl];
    [aCoder encodeObject:_commitsUrl forKey:kACClistaGistsCommitsUrl];
    [aCoder encodeObject:_user forKey:kACClistaGistsUser];
    [aCoder encodeBool:_publicProperty forKey:kACClistaGistsPublic];
    [aCoder encodeObject:_commentsUrl forKey:kACClistaGistsCommentsUrl];
}

- (id)copyWithZone:(NSZone *)zone
{
    ACClistaGists *copy = [[ACClistaGists alloc] init];
    
    if (copy) {

        copy.internalBaseClassIdentifier = [self.internalBaseClassIdentifier copyWithZone:zone];
        copy.comments = self.comments;
        copy.internalBaseClassDescription = [self.internalBaseClassDescription copyWithZone:zone];
        copy.createdAt = [self.createdAt copyWithZone:zone];
        copy.owner = [self.owner copyWithZone:zone];
        copy.url = [self.url copyWithZone:zone];
        copy.gitPushUrl = [self.gitPushUrl copyWithZone:zone];
        copy.files = [self.files copyWithZone:zone];
        copy.htmlUrl = [self.htmlUrl copyWithZone:zone];
        copy.gitPullUrl = [self.gitPullUrl copyWithZone:zone];
        copy.truncated = self.truncated;
        copy.updatedAt = [self.updatedAt copyWithZone:zone];
        copy.forksUrl = [self.forksUrl copyWithZone:zone];
        copy.commitsUrl = [self.commitsUrl copyWithZone:zone];
        copy.user = [self.user copyWithZone:zone];
        copy.publicProperty = self.publicProperty;
        copy.commentsUrl = [self.commentsUrl copyWithZone:zone];
    }
    
    return copy;
}


@end
