//
//  ACCFilesSnapshotXml.h
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface ACCFilesSnapshotXml : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *rawUrl;
@property (nonatomic, strong) NSString *language;
@property (nonatomic, strong) NSString *filename;
@property (nonatomic, assign) double size;
@property (nonatomic, strong) NSString *type;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
