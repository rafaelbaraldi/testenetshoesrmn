//
//  ACCConfigJson.m
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ACCConfigJson.h"


NSString *const kACCConfigJsonRawUrl = @"raw_url";
NSString *const kACCConfigJsonLanguage = @"language";
NSString *const kACCConfigJsonFilename = @"filename";
NSString *const kACCConfigJsonSize = @"size";
NSString *const kACCConfigJsonType = @"type";


@interface ACCConfigJson ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ACCConfigJson

@synthesize rawUrl = _rawUrl;
@synthesize language = _language;
@synthesize filename = _filename;
@synthesize size = _size;
@synthesize type = _type;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.rawUrl = [self objectOrNilForKey:kACCConfigJsonRawUrl fromDictionary:dict];
            self.language = [self objectOrNilForKey:kACCConfigJsonLanguage fromDictionary:dict];
            self.filename = [self objectOrNilForKey:kACCConfigJsonFilename fromDictionary:dict];
            self.size = [[self objectOrNilForKey:kACCConfigJsonSize fromDictionary:dict] doubleValue];
            self.type = [self objectOrNilForKey:kACCConfigJsonType fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.rawUrl forKey:kACCConfigJsonRawUrl];
    [mutableDict setValue:self.language forKey:kACCConfigJsonLanguage];
    [mutableDict setValue:self.filename forKey:kACCConfigJsonFilename];
    [mutableDict setValue:[NSNumber numberWithDouble:self.size] forKey:kACCConfigJsonSize];
    [mutableDict setValue:self.type forKey:kACCConfigJsonType];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.rawUrl = [aDecoder decodeObjectForKey:kACCConfigJsonRawUrl];
    self.language = [aDecoder decodeObjectForKey:kACCConfigJsonLanguage];
    self.filename = [aDecoder decodeObjectForKey:kACCConfigJsonFilename];
    self.size = [aDecoder decodeDoubleForKey:kACCConfigJsonSize];
    self.type = [aDecoder decodeObjectForKey:kACCConfigJsonType];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_rawUrl forKey:kACCConfigJsonRawUrl];
    [aCoder encodeObject:_language forKey:kACCConfigJsonLanguage];
    [aCoder encodeObject:_filename forKey:kACCConfigJsonFilename];
    [aCoder encodeDouble:_size forKey:kACCConfigJsonSize];
    [aCoder encodeObject:_type forKey:kACCConfigJsonType];
}

- (id)copyWithZone:(NSZone *)zone
{
    ACCConfigJson *copy = [[ACCConfigJson alloc] init];
    
    if (copy) {

        copy.rawUrl = [self.rawUrl copyWithZone:zone];
        copy.language = [self.language copyWithZone:zone];
        copy.filename = [self.filename copyWithZone:zone];
        copy.size = self.size;
        copy.type = [self.type copyWithZone:zone];
    }
    
    return copy;
}


@end
