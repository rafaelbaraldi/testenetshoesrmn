//
//  ACCInstallTxt.m
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ACCInstallTxt.h"


NSString *const kACCInstallTxtRawUrl = @"raw_url";
NSString *const kACCInstallTxtLanguage = @"language";
NSString *const kACCInstallTxtFilename = @"filename";
NSString *const kACCInstallTxtSize = @"size";
NSString *const kACCInstallTxtType = @"type";


@interface ACCInstallTxt ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ACCInstallTxt

@synthesize rawUrl = _rawUrl;
@synthesize language = _language;
@synthesize filename = _filename;
@synthesize size = _size;
@synthesize type = _type;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.rawUrl = [self objectOrNilForKey:kACCInstallTxtRawUrl fromDictionary:dict];
            self.language = [self objectOrNilForKey:kACCInstallTxtLanguage fromDictionary:dict];
            self.filename = [self objectOrNilForKey:kACCInstallTxtFilename fromDictionary:dict];
            self.size = [[self objectOrNilForKey:kACCInstallTxtSize fromDictionary:dict] doubleValue];
            self.type = [self objectOrNilForKey:kACCInstallTxtType fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.rawUrl forKey:kACCInstallTxtRawUrl];
    [mutableDict setValue:self.language forKey:kACCInstallTxtLanguage];
    [mutableDict setValue:self.filename forKey:kACCInstallTxtFilename];
    [mutableDict setValue:[NSNumber numberWithDouble:self.size] forKey:kACCInstallTxtSize];
    [mutableDict setValue:self.type forKey:kACCInstallTxtType];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.rawUrl = [aDecoder decodeObjectForKey:kACCInstallTxtRawUrl];
    self.language = [aDecoder decodeObjectForKey:kACCInstallTxtLanguage];
    self.filename = [aDecoder decodeObjectForKey:kACCInstallTxtFilename];
    self.size = [aDecoder decodeDoubleForKey:kACCInstallTxtSize];
    self.type = [aDecoder decodeObjectForKey:kACCInstallTxtType];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_rawUrl forKey:kACCInstallTxtRawUrl];
    [aCoder encodeObject:_language forKey:kACCInstallTxtLanguage];
    [aCoder encodeObject:_filename forKey:kACCInstallTxtFilename];
    [aCoder encodeDouble:_size forKey:kACCInstallTxtSize];
    [aCoder encodeObject:_type forKey:kACCInstallTxtType];
}

- (id)copyWithZone:(NSZone *)zone
{
    ACCInstallTxt *copy = [[ACCInstallTxt alloc] init];
    
    if (copy) {

        copy.rawUrl = [self.rawUrl copyWithZone:zone];
        copy.language = [self.language copyWithZone:zone];
        copy.filename = [self.filename copyWithZone:zone];
        copy.size = self.size;
        copy.type = [self.type copyWithZone:zone];
    }
    
    return copy;
}


@end
