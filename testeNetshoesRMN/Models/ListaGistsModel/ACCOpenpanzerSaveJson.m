//
//  ACCOpenpanzerSaveJson.m
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ACCOpenpanzerSaveJson.h"


NSString *const kACCOpenpanzerSaveJsonRawUrl = @"raw_url";
NSString *const kACCOpenpanzerSaveJsonLanguage = @"language";
NSString *const kACCOpenpanzerSaveJsonFilename = @"filename";
NSString *const kACCOpenpanzerSaveJsonSize = @"size";
NSString *const kACCOpenpanzerSaveJsonType = @"type";


@interface ACCOpenpanzerSaveJson ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ACCOpenpanzerSaveJson

@synthesize rawUrl = _rawUrl;
@synthesize language = _language;
@synthesize filename = _filename;
@synthesize size = _size;
@synthesize type = _type;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.rawUrl = [self objectOrNilForKey:kACCOpenpanzerSaveJsonRawUrl fromDictionary:dict];
            self.language = [self objectOrNilForKey:kACCOpenpanzerSaveJsonLanguage fromDictionary:dict];
            self.filename = [self objectOrNilForKey:kACCOpenpanzerSaveJsonFilename fromDictionary:dict];
            self.size = [[self objectOrNilForKey:kACCOpenpanzerSaveJsonSize fromDictionary:dict] doubleValue];
            self.type = [self objectOrNilForKey:kACCOpenpanzerSaveJsonType fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.rawUrl forKey:kACCOpenpanzerSaveJsonRawUrl];
    [mutableDict setValue:self.language forKey:kACCOpenpanzerSaveJsonLanguage];
    [mutableDict setValue:self.filename forKey:kACCOpenpanzerSaveJsonFilename];
    [mutableDict setValue:[NSNumber numberWithDouble:self.size] forKey:kACCOpenpanzerSaveJsonSize];
    [mutableDict setValue:self.type forKey:kACCOpenpanzerSaveJsonType];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.rawUrl = [aDecoder decodeObjectForKey:kACCOpenpanzerSaveJsonRawUrl];
    self.language = [aDecoder decodeObjectForKey:kACCOpenpanzerSaveJsonLanguage];
    self.filename = [aDecoder decodeObjectForKey:kACCOpenpanzerSaveJsonFilename];
    self.size = [aDecoder decodeDoubleForKey:kACCOpenpanzerSaveJsonSize];
    self.type = [aDecoder decodeObjectForKey:kACCOpenpanzerSaveJsonType];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_rawUrl forKey:kACCOpenpanzerSaveJsonRawUrl];
    [aCoder encodeObject:_language forKey:kACCOpenpanzerSaveJsonLanguage];
    [aCoder encodeObject:_filename forKey:kACCOpenpanzerSaveJsonFilename];
    [aCoder encodeDouble:_size forKey:kACCOpenpanzerSaveJsonSize];
    [aCoder encodeObject:_type forKey:kACCOpenpanzerSaveJsonType];
}

- (id)copyWithZone:(NSZone *)zone
{
    ACCOpenpanzerSaveJson *copy = [[ACCOpenpanzerSaveJson alloc] init];
    
    if (copy) {

        copy.rawUrl = [self.rawUrl copyWithZone:zone];
        copy.language = [self.language copyWithZone:zone];
        copy.filename = [self.filename copyWithZone:zone];
        copy.size = self.size;
        copy.type = [self.type copyWithZone:zone];
    }
    
    return copy;
}


@end
