//
//  ACCGistfile1Txt.m
//
//  Created by Rodrigo Mendes Nunes on 01/06/16
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import "ACCGistfile1Txt.h"


NSString *const kACCGistfile1TxtRawUrl = @"raw_url";
NSString *const kACCGistfile1TxtLanguage = @"language";
NSString *const kACCGistfile1TxtFilename = @"filename";
NSString *const kACCGistfile1TxtSize = @"size";
NSString *const kACCGistfile1TxtType = @"type";


@interface ACCGistfile1Txt ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ACCGistfile1Txt

@synthesize rawUrl = _rawUrl;
@synthesize language = _language;
@synthesize filename = _filename;
@synthesize size = _size;
@synthesize type = _type;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
            self.rawUrl = [self objectOrNilForKey:kACCGistfile1TxtRawUrl fromDictionary:dict];
            self.language = [self objectOrNilForKey:kACCGistfile1TxtLanguage fromDictionary:dict];
            self.filename = [self objectOrNilForKey:kACCGistfile1TxtFilename fromDictionary:dict];
            self.size = [[self objectOrNilForKey:kACCGistfile1TxtSize fromDictionary:dict] doubleValue];
            self.type = [self objectOrNilForKey:kACCGistfile1TxtType fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.rawUrl forKey:kACCGistfile1TxtRawUrl];
    [mutableDict setValue:self.language forKey:kACCGistfile1TxtLanguage];
    [mutableDict setValue:self.filename forKey:kACCGistfile1TxtFilename];
    [mutableDict setValue:[NSNumber numberWithDouble:self.size] forKey:kACCGistfile1TxtSize];
    [mutableDict setValue:self.type forKey:kACCGistfile1TxtType];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description 
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];

    self.rawUrl = [aDecoder decodeObjectForKey:kACCGistfile1TxtRawUrl];
    self.language = [aDecoder decodeObjectForKey:kACCGistfile1TxtLanguage];
    self.filename = [aDecoder decodeObjectForKey:kACCGistfile1TxtFilename];
    self.size = [aDecoder decodeDoubleForKey:kACCGistfile1TxtSize];
    self.type = [aDecoder decodeObjectForKey:kACCGistfile1TxtType];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_rawUrl forKey:kACCGistfile1TxtRawUrl];
    [aCoder encodeObject:_language forKey:kACCGistfile1TxtLanguage];
    [aCoder encodeObject:_filename forKey:kACCGistfile1TxtFilename];
    [aCoder encodeDouble:_size forKey:kACCGistfile1TxtSize];
    [aCoder encodeObject:_type forKey:kACCGistfile1TxtType];
}

- (id)copyWithZone:(NSZone *)zone
{
    ACCGistfile1Txt *copy = [[ACCGistfile1Txt alloc] init];
    
    if (copy) {

        copy.rawUrl = [self.rawUrl copyWithZone:zone];
        copy.language = [self.language copyWithZone:zone];
        copy.filename = [self.filename copyWithZone:zone];
        copy.size = self.size;
        copy.type = [self.type copyWithZone:zone];
    }
    
    return copy;
}


@end
