//
//  BaseManager.m
//  testeNetshoesRMN
//
//  Created by Rodrigo Mendes Nunes on 01/06/16.
//  Copyright © 2016 Rodrigo Mendes Nunes. All rights reserved.
//

#import "BaseManager.h"
#import <AFNetworking/AFNetworking.h>
#import "ConstantsHelper.h"
//#import "PullRequestsDataModels.h"

@implementation BaseManager

+ (void)getGists:(int)page onSucces:(void (^)(NSArray* respObj))successFuction
                onError:(void (^)(id err))errorFuction
{
    // Realiza a request utilizando a biblioteca -> AFNetworking
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSString* urlString = [NSString stringWithFormat:@"https://api.github.com/gists/public?page=%d", page];
    
    NSURL *URL = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
            
            errorFuction(error);
            
        } else {
            NSLog(@"%@ %@", response, responseObject);
            
            successFuction( responseObject );
        }
    }];
    [dataTask resume];
}

+ (void)getGistRaw:(NSString*)rawUrl onSuccess:(void (^)(id respObj))successFuction
                onError:(void (^)(id err))errorFuction
{
    // Realiza a request utilizando a biblioteca -> AFNetworking
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];

    NSString* urlString = [NSString stringWithFormat:@"%@", rawUrl];
    
    NSURL *URL = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
            
            errorFuction(error);
            
        } else {
            NSLog(@"%@ %@", response, responseObject);
            
            successFuction( responseObject );
        }
    }];
    [dataTask resume];
}

@end
