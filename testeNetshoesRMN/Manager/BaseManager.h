//
//  BaseManager.h
//  testeNetshoesRMN
//
//  Created by Rodrigo Mendes Nunes on 01/06/16.
//  Copyright © 2016 Rodrigo Mendes Nunes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataModelsGists.h"

@interface BaseManager : NSObject

+ (void)getGists:(int)page onSucces:(void (^)(NSArray* respObj))successFuction
         onError:(void (^)(id err))errorFuction;


+ (void)getGistRaw:(NSString*)rawUrl onSuccess:(void (^)(id respObj))successFuction
                onError:(void (^)(id err))errorFuction;

@end
